<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: braska
  Date: 10/8/15
  Time: 12:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<h1>Test Servlet</h1>

<form action="" method="post">
    <input type="text" name="str"/>
    <button type="submit">ОК</button>
</form>
<% List<String> list = (List<String>) session.getAttribute("list"); %>
<% if (list != null && !list.isEmpty()) { %>
<ul>
    <% for (String aList : list) { %>
    <li><%= aList %></li>
    <% } %>
</ul>
<% } else { %>
<p><i>Пусто</i></p>
<% } %>
</body>
</html>
