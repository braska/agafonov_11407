package ru.kpfu.agafonov;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        Pattern p2 = Pattern.compile(".*name.*");
        Pattern p3a = Pattern.compile("a+b*");
        Pattern p3b = Pattern.compile("\\\\*\\**");
        String param = "ololo";
        Pattern p3c = Pattern.compile("(" + param + "){3}");
        Pattern p3d = Pattern.compile("(.|\n){5}");

        Pattern p4 = Pattern.compile("[0-9]{3}-[0-9]{2}-[0-9]{2}");
        Pattern p5 = Pattern.compile("\\+7 \\([0-9]{3}\\) [0-9]{3}-[0-9]{2}-[0-9]{2}");
        Pattern p6 = Pattern.compile("[А-ЯЁ][а-яё]* [А-ЯЁ][а-яё]* [А-ЯЁ][а-яё]*");
        Pattern p7 = Pattern.compile("(?!\\.)[-+_a-zа-яё0-9.]++(?<!\\.)@(?![-.])[-a-zа-яё0-9.]+(?<!\\.)\\.[a-zа-яё]{2,6}", Pattern.CASE_INSENSITIVE);

        Pattern p8 = Pattern.compile("([0-9]+):([a-zа-яё]*)(\n|$)");
        Matcher m8 = p8.matcher("123:sdf\n234:dfg\n567:sdf");
        Map<String, String> map8 = new HashMap<>();
        while (m8.find()) {
            System.out.println(m8.group(1) + ":" + m8.group(2));
            map8.put(m8.group(1), m8.group(2));
        }

        Pattern p9 = Pattern.compile("\\<a href=\\\"(.*?)\\\">(.*?)\\</a\\>", Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        Matcher m9 = p9.matcher("asdafgwewfybhsdfh asdad34 3645 sd3t3t <a href=\"test.html\">234234</a> asdasd asda23gs wr w 3 fdf s <a href=\"http://ya.ru\">dfsdf</a>");
        while (m9.find()) {
            System.out.println(m9.group());
        }

        // Задание 10
        System.out.println(m9.replaceAll("123"));
        // Задачние 11
        System.out.println(m9.replaceAll("$1"));

        // Задание 12
        System.out.println(m9.replaceAll("<a class=\"$1\">$2</a>"));

        // Задание 13
        Pattern p13 = Pattern.compile("\\<a href=\\\"(.*?)\\\" class=\\\"(.*?)\\\">(.*?)\\</a\\>", Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
        Matcher m13 = p13.matcher("asdafgwewfybhsdfh asdad34 3645 sd3t3t <a href=\"test.html\" class=\"test-class\">234234</a> asdasd asda23gs wr w 3 fdf s <a href=\"http://ya.ru\">dfsdf</a>");
        System.out.println(m13.replaceAll("<a href=\"$2\" class=\"$1\">$3</a>"));

        // Задание 14
        System.out.println(m13.replaceAll("$3"));

        // Задание 15
        Pattern p15 = Pattern.compile("(//.*)|(/\\*(.|\\n)*?\\*/)", Pattern.MULTILINE);
        Matcher m15 = p15.matcher("// asfadasdasdasdasd\nasdasdwevrwrwev534v534 34534 vv534 5\nw34cwre4f43 t\n/* dasdedad\nasdasd23r*/");
        System.out.println(m15.replaceAll(""));

        // Задание 16
        Pattern p16 = Pattern.compile("(.*?);(.*?);(.*?);(.*?);", Pattern.MULTILINE);
        Matcher m16 = p16.matcher("100;10;name;zero;\n100;10;name;zero;\n100;10;name;zero;\n100;10;name;zero;");
        System.out.println(m16.replaceAll("$1;$3;$4;"));

        // Задание 17
        System.out.println(m16.replaceAll("$1;$2 RUB;$3;$4;"));
    }
}
