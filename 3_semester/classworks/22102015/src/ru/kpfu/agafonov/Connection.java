package ru.kpfu.agafonov;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Connection {
    private static volatile java.sql.Connection instance;

    public static synchronized java.sql.Connection getInstance() {
        return instance;
    }

    public static synchronized void connect() throws SQLException, ClassNotFoundException {
        if (instance == null) {
            String url = "jdbc:postgresql://127.0.0.1:5432/informatic_classwork_22102015";
            String name = "postgres";
            String password = "";
            Class.forName("org.postgresql.Driver");
            instance = DriverManager.getConnection(url, name, password);
        }
    }
}
