package ru.kpfu.agafonov;

import ru.kpfu.agafonov.models.ComparsionOperator;
import ru.kpfu.agafonov.models.Department;
import ru.kpfu.agafonov.models.Employee;

import java.sql.SQLException;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        try {
            Connection.connect();

            System.out.println("Задание №1");
            List<Employee> task1 = Employee.findAllByWage(ComparsionOperator.GREATER, 20000);
            for(Employee model : task1) {
                System.out.println(model.getName());
            }
            System.out.println();

            System.out.println("Задание №2");
            List<Department> task2 = Department.findAllByEmplyeeCount(ComparsionOperator.GREATER, 20);
            for(Department model : task2) {
                System.out.println(model.getName());
            }
            System.out.println();

            System.out.println("Задание №3");
            List<Employee> task3 = Employee.findAllByDepartmentAndPosition("Отдел продаж", "Менеджер");
            for(Employee model : task3) {
                System.out.println(model.getName());
            }
            System.out.println();

            System.out.println("Задание №4");
            List<Employee> task4 = Employee.findAllRetiree();
            for(Employee model : task4) {
                System.out.println(model.getName());
            }
            System.out.println();

            System.out.println("Задание №5");
            List<Employee> task5 = Employee.findAllBySubordinatesCount(ComparsionOperator.GREATER, 5);
            for(Employee model : task5) {
                System.out.println(model.getName());
            }
            System.out.println();

            System.out.println("Задание №6");
            Employee.deleteAllByDepartmentAndPositionAndWage("Отдел продаж", "Менеджер", 19000, ComparsionOperator.GREATER);
            System.out.println("Удалено!");
            System.out.println();

            System.out.println("Задание №7");
            Employee.increaseWageByName("Мужик, 105 лет, зарплата = 99999", 100);
            System.out.println("Зарплата повышена!");
            System.out.println();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
