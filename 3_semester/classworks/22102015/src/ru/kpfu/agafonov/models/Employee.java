package ru.kpfu.agafonov.models;

import ru.kpfu.agafonov.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Employee {
    private Long id;
    private String name;
    private Date birthday;
    private String phone;
    private String address;
    private Gender gender;
    private Integer wage;
    private Long head_id;
    private Long departmentId;
    private Long positionId;

    private Employee(Long id, String name, Date birthday, String phone, String address, Gender gender, Integer wage, Long head_id, Long department_id, Long position_id) {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
        this.phone = phone;
        this.address = address;
        this.gender = gender;
        this.wage = wage;
        this.head_id = head_id;
        this.departmentId = department_id;
        this.positionId = position_id;
    }


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public Gender getGender() {
        return gender;
    }

    public Integer getWage() {
        return wage;
    }

    public Long getHead_id() {
        return head_id;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public Long getPositionId() {
        return positionId;
    }

    private static List<Employee> getListFromResultSet(ResultSet result) throws SQLException {
        List<Employee> list = new LinkedList<>();
        while (result.next()) {
            String gender = result.getString("gender");
            Employee employee = new Employee(
                    result.getLong("id"),
                    result.getString("name"),
                    result.getDate("birthday"),
                    result.getString("phone"),
                    result.getString("address"),
                    gender != null ? Gender.valueOf(result.getString("gender").toUpperCase()) : null,
                    result.getInt("wage"),
                    result.getLong("head_id"),
                    result.getLong("department_id"),
                    result.getLong("position_id")
            );
            list.add(employee);
        }
        return list;
    }

    public static List<Employee> findAllByWage(ComparsionOperator operator, Integer wage) throws SQLException {
        PreparedStatement statement = Connection.getInstance().prepareStatement("SELECT * FROM employees WHERE wage " + operator.getValue() + " ?");
        statement.setInt(1, wage);

        return getListFromResultSet(statement.executeQuery());
    }

    /*
    На практике никогда такого не будет. Всегда будет выборка по ID, а не по строковым значениям
     */
    public static List<Employee> findAllByDepartmentAndPosition(String department, String position) throws SQLException {
        PreparedStatement statement = Connection.getInstance().prepareStatement("SELECT * FROM employees JOIN departments ON (departments.id = employees.department_id AND departments.name = ?) JOIN positions ON (positions.id = employees.position_id AND positions.name = ?)");
        statement.setString(1, department);
        statement.setString(2, position);
        return getListFromResultSet(statement.executeQuery());
    }

    public static List<Employee> findAllRetiree() throws SQLException {
        PreparedStatement statement = Connection.getInstance().prepareStatement("SELECT * FROM employees WHERE (gender = ?::gender AND age(birthday) > ?::INTERVAL) OR (gender = ?::gender AND age(birthday) > ?::INTERVAL)");

        statement.setString(1, Gender.MALE.getValue());
        statement.setString(2, 60 + " year");

        statement.setString(3, Gender.FEMALE.getValue());
        statement.setString(4, 55 + " year");

        return getListFromResultSet(statement.executeQuery());
    }

    public static List<Employee> findAllBySubordinatesCount(ComparsionOperator operator, Integer count) throws SQLException {
        PreparedStatement statement = Connection.getInstance().prepareStatement("SELECT heads.* FROM employees AS heads JOIN employees AS subordinates ON heads.id = subordinates.head_id GROUP BY heads.id HAVING COUNT(subordinates.id) " + operator.getValue() + " ?");
        statement.setInt(1, count);
        return getListFromResultSet(statement.executeQuery());
    }

    /*
    1) На практике никогда такого не будет. Всегда будет выборка по ID, а не по строковым значениям.
    2) DELETE не поддерживает JOIN (в mysql (включая mariaDB и percona) и в postges точно, в остальных не знаю)
     */
    public static void deleteAllByDepartmentAndPositionAndWage(String department, String position, Integer wage, ComparsionOperator operator) throws SQLException {
        PreparedStatement statement = Connection.getInstance().prepareStatement("DELETE FROM employees WHERE id IN (SELECT e.id FROM employees AS e JOIN departments ON (departments.id = e.department_id AND departments.name = ?) JOIN positions ON (positions.id = e.position_id AND positions.name = ?) WHERE e.wage " + operator.getValue() + " ?)");
        statement.setString(1, department);
        statement.setString(2, position);
        statement.setInt(3, wage);
        statement.execute();
    }

    public static void increaseWageByName(String name, Integer percent) throws SQLException {
        PreparedStatement statement = Connection.getInstance().prepareStatement("UPDATE employees SET wage = wage + wage * ? WHERE name = ?");
        statement.setInt(1, percent / 100);
        statement.setString(2, name);

        statement.execute();
    }
}
