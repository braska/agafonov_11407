package ru.kpfu.agafonov.models;

public enum ComparsionOperator {
    GREATER(">"),
    GREATER_OR_EQUAL(">="),
    LOWER("<"),
    LOWER_OR_EQUAL("<="),
    EQUAL("="),
    NOT_EQUAL("<>");

    private String value;

    ComparsionOperator(String i) {
        this.value = i;
    }

    public String getValue() {
        return value;
    }
}
