package ru.kpfu.agafonov.models;

public enum Gender {
    MALE("male"),
    FEMALE("female");

    private String value;

    Gender(String i) {
        this.value = i;
    }

    public String getValue() {
        return value;
    }
}
