package ru.kpfu.agafonov.models;

import ru.kpfu.agafonov.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class Department {
    private Long id;
    private String name;

    private Department(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static List<Department> findAllByEmplyeeCount(ComparsionOperator operator, Integer count) throws SQLException {
        List<Department> list = new LinkedList<>();
        PreparedStatement statement = Connection.getInstance().prepareStatement("SELECT departments.* FROM departments JOIN employees ON employees.department_id = departments.id GROUP BY departments.id HAVING COUNT(employees.id) " + operator.getValue() + " ?");
        statement.setInt(1, count);
        ResultSet result = statement.executeQuery();
        while (result.next()) {
            Department department = new Department(
                    result.getLong("id"),
                    result.getString("name")
            );
            list.add(department);
        }
        return list;
    }
}
