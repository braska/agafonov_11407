CREATE TABLE departments (
  id       BIGSERIAL PRIMARY KEY,
  name     VARCHAR(255) NOT NULL
);

CREATE TABLE duties (
  id       BIGSERIAL PRIMARY KEY,
  name     VARCHAR(255) NOT NULL
);

CREATE TABLE positions (
  id       BIGSERIAL PRIMARY KEY,
  name     VARCHAR(255) NOT NULL
);

CREATE TABLE positions_duties (
  position_id   BIGINT REFERENCES positions(id) NOT NULL,
  dutie_id      BIGINT REFERENCES duties(id) NOT NULL,
  PRIMARY KEY(position_id, dutie_id)
);

CREATE TYPE gender AS ENUM ('male', 'female');

CREATE TABLE employees (
  id            BIGSERIAL PRIMARY KEY,
  name          VARCHAR(255) NOT NULL,
  birthday      DATE,
  phone         VARCHAR(255),
  address       VARCHAR(255),
  gender        gender,
  wage          INTEGER,
  head_id       BIGINT REFERENCES employees(id),
  department_id BIGINT REFERENCES departments(id),
  position_id   BIGINT REFERENCES positions(id)
);
