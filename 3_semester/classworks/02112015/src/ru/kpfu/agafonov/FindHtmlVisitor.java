package ru.kpfu.agafonov;

import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindHtmlVisitor extends SimpleFileVisitor<Path> {
    private Consumer<Path> callback;


    public FindHtmlVisitor(Consumer<Path> callback) {
        super();
        this.callback = callback;
    }

    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        Objects.requireNonNull(file);
        Objects.requireNonNull(attrs);

        Pattern pattern = Pattern.compile(".*\\.html");
        Matcher matcher = pattern.matcher(file.getFileName().toString());

        if (matcher.matches()) {
            callback.accept(file);
        }

        return FileVisitResult.CONTINUE;
    }
}
