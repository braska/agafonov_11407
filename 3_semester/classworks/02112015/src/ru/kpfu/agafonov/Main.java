package ru.kpfu.agafonov;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {
        //copyFolder(Paths.get("./test"), Paths.get("./test2"));
        findHtml(Paths.get("./test"));
    }

    public static void copyFolder(Path source, Path destination) {
        try {
            Files.walkFileTree(source, new CopyFileVisitor(source, destination));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void findHtml(Path path) {
        try {
            Files.walkFileTree(path, new FindHtmlVisitor(System.out::println));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
