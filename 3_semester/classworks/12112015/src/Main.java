import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        String str = "(2+3*9)+(3+6*7)*(7+9*6)";
        recursiveCalc(str);
    }

    private static int recursiveCalc(String str) {
        Pattern p = Pattern.compile("[^\\(]*(\\(.*\\))[^\\)]*");
        Matcher m = p.matcher(str);
        while(m.find()) {
            System.out.println(m.group());
        }
        return 0;
    }
}
