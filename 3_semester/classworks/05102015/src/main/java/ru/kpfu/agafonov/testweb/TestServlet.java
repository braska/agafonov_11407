package ru.kpfu.agafonov.testweb;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Gataullin Kamil
 *         18.10.2014 18:39:33
 */
public class TestServlet extends HttpServlet {

    private int fieldCounter;

    @Override
    public void init() throws ServletException {
        fieldCounter = 0;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "TestGet";
        }
        switch (action) {
            case "TestGet":
                testGet(response);
                break;
            case "TestField":
                testField(response);
                break;
            case "TestInitParam":
                testInitParam(response);
                break;
            case "TestSession":
                testSession(request, response);
                break;
            case "TestForward":
                testForward(request, response);
                break;
            case "TestRedirect":
                testRedirect(response);
                break;
            case "TestCookie":
                testCookie(request, response);
                break;
            default:
                testGet(response);
        }
    }

    private void testRedirect(HttpServletResponse response) throws IOException {
        response.sendRedirect("http://yandex.ru");
    }

    private void testForward(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("forward.html");
        requestDispatcher.forward(request, response);
    }

    private void testSession(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        Integer sessionCounter = (Integer) session.getAttribute("sessionCounter");
        if (sessionCounter == null) {
            sessionCounter = 0;
        }
        sessionCounter++;
        session.setAttribute("sessionCounter", sessionCounter);

        response.setContentType("text/html");
        response.getWriter().print(sessionCounter);
        response.getWriter().close();
    }

    private void testInitParam(HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        response.getWriter().print(getInitParameter("MyInitParam"));
        response.getWriter().close();
    }

    private void testField(HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        response.getWriter().print(++fieldCounter);
        response.getWriter().close();
    }

    private void testGet(HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        response.getWriter().print("GET");
        response.getWriter().close();
    }

    private void testCookie(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Cookie counterCookie = null;
        int counter = 0;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("counterCookie")) {
                    counterCookie = cookie;
                    break;
                }
            }
        }
        if (counterCookie == null) {
            counterCookie = new Cookie("counterCookie", "" + counter);
            counterCookie.setMaxAge(60 * 60 * 24 * 14);
        } else {
            counter = Integer.parseInt(counterCookie.getValue());
            counter++;
            counterCookie.setValue("" + counter);
            counterCookie.setMaxAge(60 * 60 * 24 * 14);
        }

        response.addCookie(counterCookie);

        response.setContentType("text/html");
        response.getWriter().print(counter);
        response.getWriter().close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String value = request.getParameter("MyRequestParam");
        response.setContentType("text/html");
        response.getWriter().print(value);
        response.getWriter().close();
    }
}