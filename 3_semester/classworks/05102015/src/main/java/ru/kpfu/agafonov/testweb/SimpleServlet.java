package ru.kpfu.agafonov.testweb;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Gataullin Kamil
 * 01.10.2015 23:44
 */
public class SimpleServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        pw.println("<!DOCTYPE html>");
        pw.println("<html>");
        pw.println("<head>");
        pw.println("    <title>Test Java Web Application</title>");
        pw.println("</head>");
        pw.println("<body>");
        pw.println("<div align=\"center\">");
        pw.println("    <h1>");
        pw.println("        Test Java Web Application Simple Servlet");
        pw.println("    </h1>");
        pw.println("</div>");
        pw.println("</body>");
        pw.println("</html>");
        pw.close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.getWriter().print("POST request");
        response.getWriter().close();
    }

    @Override
    public void destroy() {
    }
}
