var checkNum = function(str, temp) {
  return temp ? !!str.match(/^\+?[0-9]{0,11}$/) : !!str.match(/^\+?[0-9]{11}$/);
};

jQuery.fn.getCursorPosition = function(){
	if(this.lengh == 0) return -1;
	return $(this).getSelectionStart();
};

jQuery.fn.getSelectionStart = function(){
	if(this.lengh == 0) return -1;
	input = this[0];

	var pos = input.value.length;

	if (input.createTextRange) {
		var r = document.selection.createRange().duplicate();
		r.moveEnd('character', input.value.length);
		if (r.text == '')
		pos = input.value.length;
		pos = input.value.lastIndexOf(r.text);
	} else if(typeof(input.selectionStart)!="undefined")
	pos = input.selectionStart;

	return pos;
};

$(document).ready(function() {
  var input = $('input[type=text]');
  input.on('keypress paste', function(e) {
    var char = String.fromCharCode(e.charCode);
    if (!checkNum(input.val().substr(0, input.getCursorPosition()) + char + input.val().substr(input.getCursorPosition()), true)) {
      e.preventDefault();
    }
  });
  input.on('blur', function() {
    if (!checkNum(input.val())) {
      alert("Неверный номер!");
    }
  });
});
