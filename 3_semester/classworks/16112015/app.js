var promptInt = function(str, callback) {
  var age = NaN;
  while(isNaN(age)) {
    age = parseInt(prompt(str));
  }
  callback(age);
}

promptInt("Сколько тебе лет?", function(age) {
    if (age <= 6) {
      alert("Дошкольник");
    } else if (age <= 18) {
      alert("Школьник");
    } else if (age <= 22) {
      alert("Студент");
    } else {
      alert("Взрослый");
    }
});

//------------------------------------------------------------------------------
var fact = function(num) {
  return num ? num * this.call(this, num - 1) : 1;
};

alert(fact.call(fact, 3));
