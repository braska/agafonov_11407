<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: Лия
  Date: 08.10.2015
  Time: 13:34
  To change this template use File | Settings | File Templates.
--%>
<%
    List<String> list = (List<String>) request.getAttribute("list");
    String errText = (String) request.getAttribute("Error");
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
</head>
<body>
<c:out value="hello world"/>
<form ACTION="${pageContext.request.contextPath}/test" method="post">
    <label>
        <input type="text" name="Name">
    </label>
    <label>
        <input type="submit" name="button" value="OK">
    </label>
</form>
<% if (errText != null){%>
    <p style="color: #ff4bf6;"><%=errText%></p>
<%}%>
<% if (list == null || list.isEmpty()) {%>
    List is empty
<%} else {%>
<ul><% for (String i : list) {%>
    <li>
        <%=i%>
    </li>
    <%}%>
</ul>
<%}%>
</body>
</html>
