var clone = function(obj) {
  var newObj = {};
  for (var field in obj) {
    if (typeof obj[field] == "object") {
      newObj[field] = clone(obj[field]);
    } else {
      newObj[field] = obj[field];
    }
  }
  return newObj;
};


var sum = function() {
  var sum = 0;
  for (var i = 0; i < arguments.length; i++) {
    sum += arguments[i];
  }
  return sum;
};
