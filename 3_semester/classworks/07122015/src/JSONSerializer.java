import org.json.simple.JSONObject;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JSONSerializer {
    private Object object;
    private Class<?> clazz;
    public JSONSerializer(Object object) {
        this.object = object;
        this.clazz = this.object.getClass();
        if (!this.clazz.isAnnotationPresent(JSONable.class)) {
            throw new IllegalArgumentException("Object must be annotated with @JSONAble");
        }
    }

    public String serialize() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Field[] fields = this.clazz.getDeclaredFields();
        JSONObject jsonObject = new JSONObject();
        for (Field field : fields) {
            if (field.isAnnotationPresent(JSONignore.class)) {
                continue;
            }
            Class<?> fieldClass = field.getDeclaringClass();
            String name = field.getName();

            if (field.isAnnotationPresent(JSONname.class)) {
                Annotation annotation = field.getAnnotation(JSONname.class);
                Class<?> annClass = annotation.getClass();
                name = (String) annClass.getMethod("value").invoke(annotation);
            }
            if (field.isAnnotationPresent(DateFormat.class)) {
                if (field.getType() != Date.class) {
                    throw new IllegalArgumentException("Only date can be annotated with @DateFormat");
                }
                Annotation annotation = field.getAnnotation(DateFormat.class);
                Class<?> annClass = annotation.getClass();
                String dateFormat = (String) annClass.getMethod("value").invoke(annotation);
                SimpleDateFormat format = new SimpleDateFormat(dateFormat);
                jsonObject.put(name, format.format((Date) field.get(this.object)));

            } else {
                jsonObject.put(name, field.get(object));
            }
        }
        return jsonObject.toJSONString();
    }
}
