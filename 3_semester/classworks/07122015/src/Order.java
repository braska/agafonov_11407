import java.util.Date;

@JSONable
public class Order {
    String name;

    @JSONname("number")
    int count;

    @DateFormat("dd.mm.YYYY")
    Date d;

    @JSONignore
    double ignore;

    public Order(String name, int count, Date d, double ignore) {
        this.name = name;
        this.count = count;
        this.d = d;
        this.ignore = ignore;
    }
}
