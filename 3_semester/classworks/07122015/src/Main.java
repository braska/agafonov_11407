import java.lang.reflect.InvocationTargetException;
import java.util.Date;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Order testOrder = new Order("Тестовое имя", 123, new Date(), 13.3);
        JSONSerializer serializer = new JSONSerializer(testOrder);
        System.out.println(serializer.serialize());
    }
}
