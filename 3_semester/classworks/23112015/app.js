function Ladder() {
  this.step = 1;
}

Ladder.prototype.up = function () {
  this.step++;
  return this;
};

Ladder.prototype.down = function () {
  this.step--;
  return this;
};

Ladder.prototype.show = function () {
  alert(this.step);
  return this;
};

var ladder = new Ladder();
ladder.up().down().up().up().show();

//---------------------------------
function Calc() {
  this.sum = function() {
    var sum = 0;
    for (var i = 0; i < arguments.length; i++) {
      sum += arguments[i];
    }
    return sum;
  };

  this.multiply = function() {
    var result = 0;
    for (var i = 0; i < arguments.length; i++) {
      result *= arguments[i];
    }
    return result;
  };
}

var myCalc = new Calc();
alert(myCalc.sum(4, 7, 10));

//----------------------------------------
function Animal() {
  var name;
  this.getSet.name = function(newName) {
    if (newName != undefined) {
      if (typeof newName == "string") {
        name = newName;
      }
    } else {
      return name;
    }
  };
}
