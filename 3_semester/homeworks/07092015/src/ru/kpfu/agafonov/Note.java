package ru.kpfu.agafonov;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class Note {
    private String name;
    private String phone;
    private LocalDate birthday;

    public Note(String name, String phone, String birthday) throws DateTimeParseException {
        this.name = name;
        this.phone = phone;
        this.birthday = LocalDate.parse(birthday, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public LocalDate getBirthday() {
        return birthday;
    }
}
