package ru.kpfu.agafonov;

import java.util.List;

public interface Notebook {
    void add(Note n);
    boolean remove(String name);
    List<Note> getAll();
    Note getByName(String name);
    List<Note> getByFirstChar(Character character);
}
