package ru.kpfu.agafonov;

public class Main {
    public static void main(String[] args) {
        Notebook notebook = new NotebookImpl();
        notebook.add(new Note("Иванов", "+79876543210", "14.12.1995"));
        notebook.add(new Note("Агафонов", "+79003267271", "14.12.1995"));

        for (Note note : notebook.getAll()) {
            System.out.println(note.getName());
        }
    }
}
