package ru.kpfu.agafonov;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class NotebookImpl implements Notebook {
    /*
    Очень плохой вариант хранения. Но лучше только с БД. Тут это нужно как никогда.
     */
    private Map<Character, Map<String, Note>> store = new TreeMap<>();

    @Override
    public void add(Note n) {
        String name = n.getName();
        Character character = name.charAt(0);
        if (store.containsKey(character)) {
            store.get(character).put(name, n);
        } else {
            Map<String, Note> map = new TreeMap<>();
            map.put(name, n);
            store.put(character, map);
        }
    }

    @Override
    public boolean remove(String name) {
        Character character = name.charAt(0);
        if (store.containsKey(character) && store.get(character).containsKey(name)) {
            store.get(character).remove(name);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<Note> getAll() {
        List<Note> list = new ArrayList<>();
        for (Map<String, Note> noteMap : store.values()) {
            list.addAll(noteMap.values());
        }
        return list;
    }

    @Override
    public Note getByName(String name) {
        Character character = name.charAt(0);
        return store.containsKey(character) ? store.get(character).get(name) : null;
    }

    @Override
    public List<Note> getByFirstChar(Character character) {
        return store.containsKey(character) ? new ArrayList<>(store.get(character).values()) : new ArrayList<>();
    }
}
