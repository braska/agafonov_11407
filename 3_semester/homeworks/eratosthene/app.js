function getSieveOfEratosthenes(n){
	// здесь ключ - это число, значение - логическое значение означающее простоту числа
	var sieve = [];
	sieve[1] = false; // 1 - не простое число
	for(var k = 2; k <= n; k++) {
		sieve[k] = true;
	}

	for(var k = 2; k * k <= n; k++) {
		// если k - не вычеркнуто
		if(sieve[k]){
			// то вычеркнем кратные k
			for(var l = k * k; l <= n; l += k){
				sieve[l] = false;
			}
		}
	}
	return sieve;
}

var sieve = getSieveOfEratosthenes(120);
for (var i = 0; i < sieve.length; i++) {
	document.writeln("<strong>" + i + "</strong> &mdash; " + (sieve[i] ? "Простое" : "Не простое") + "<br>");
}
