<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head lang="ru">
    <meta charset="UTF-8">
    <title>Записная книжка</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
</head>
<body>
<div class="container-fluid">

    <div class="card">
        <div class="row">
            <div class="col-md-5">
                <form class="form-horizontal" id="form" method="post" action="">
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">Имя</label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name" placeholder="Имя" name="name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">E-mail</label>

                        <div class="col-sm-9">
                            <input type="email" class="form-control" id="email" name="email" placeholder="E-mail">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-sm-3 control-label">Номер телефона</label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="phone" name="phone"
                                   placeholder="+7 ... ...-..-..">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="note" class="col-sm-3 control-label">Заметка</label>

                        <div class="col-sm-9">
                            <textarea name="note" id="note" rows="10" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-7">
                <h1>Телефонная книжка</h1>

                <p>Данная страница представляет собой электронную записную книжку, в которой можно хранить имена, даты
                    рождения, адреса,телефоны и другую контактную информацию Ваших знакомых и друзей.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="list">
            <c:forEach var="man" items="${people}">
                <div class="col-md-6">
                    <div class="card" data-id="${man.id}">
                        <h1>${man.name}</h1>
                        <dl class="dl-horizontal">
                            <c:if test="${not empty man.email}">
                                <dt>E-mail</dt>
                                <dd>${man.email}</dd>
                            </c:if>

                            <c:if test="${not empty man.phone}">
                                <dt>Номер телефона</dt>
                                <dd>${man.phone}</dd>
                            </c:if>

                            <c:if test="${not empty man.note}">
                                <dt>Заметка</dt>
                                <dd>${man.note}</dd>
                            </c:if>
                        </dl>
                    </div>
                </div>
            </c:forEach>
            <c:if test="${empty people}">
                <div class="col-md-12">
                    <p class="none">Пусто</p>
                </div>
            </c:if>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="http://yastatic.net/underscore/1.6.0/underscore-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.2.2/isotope.pkgd.min.js"></script>
<script src="${pageContext.request.contextPath}/app.js"></script>
</body>
</html>
