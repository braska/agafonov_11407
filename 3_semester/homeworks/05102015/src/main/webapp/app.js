$(window).load(function () {
    $('#list').isotope({
        itemSelector: '.col-md-6'
    })
});

$(document).ready(function () {
    $('#list').on('click', 'card', function () {
        document.location = '/users/' + $(this).data('id');
    });
});