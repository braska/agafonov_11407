package ru.kpfu.agafonov.testweb.models;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ManJDBC implements Serializable {
    private static Connection connection;

    static {
        createConnection();
    }

    public static List<Man> getAll() throws SQLException {
        List<Man> people = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM people;");
        while (resultSet.next()) {
            Man man = new Man(resultSet.getString("name"), resultSet.getString("email"), resultSet.getString("phone"), resultSet.getString("note"));
            people.add(man);
        }
        return people;
    }

    public static void add(Man man) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("INSERT INTO people (name, email, phone, note) VALUES (?, ?, ?, ?);");
        statement.setString(1, man.getName());
        statement.setString(2, man.getEmail());
        statement.setString(3, man.getPhone());
        statement.setString(4, man.getNote());

        statement.execute();
    }

    public static void removeById(Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("DELETE FROM people WHERE name = ?;");

        statement.setLong(1, id);

        statement.execute();
    }

    public static Man getById(Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM people WHERE name = ?;");

        statement.setLong(1, id);

        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            return new Man(resultSet.getString("name"), resultSet.getString("email"), resultSet.getString("phone"), resultSet.getString("note"));
        } else {
            return null;
        }
    }

    private static void createConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/notebook", "postgres", "");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
