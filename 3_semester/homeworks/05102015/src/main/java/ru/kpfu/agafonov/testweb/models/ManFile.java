package ru.kpfu.agafonov.testweb.models;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ManFile implements Serializable {
    private transient static ObjectInputStream input;
    private transient static ObjectOutputStream output;

    static {
        try {
            output = new ObjectOutputStream(new FileOutputStream("./people"));
            input = new ObjectInputStream(new FileInputStream("./people"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Man> getAll() {
        try {
            List<Man> list = (List<Man>) input.readObject();
            return list;
        } catch (EOFException e) {
            return new ArrayList<>();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static void add(Man man) {
        List<Man> list = null;
        try {
            try {
                list = (List<Man>) input.readObject();
            } catch (EOFException e) {
                list = new ArrayList<>();
            }
            System.out.println(Arrays.toString(list.toArray()));
            list.add(man);
            output.writeObject(list);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void removeById(Long id) throws SQLException {
        throw new UnsupportedOperationException();
    }

    public static Man getById(Long id) throws SQLException {
        throw new UnsupportedOperationException();
    }
}
