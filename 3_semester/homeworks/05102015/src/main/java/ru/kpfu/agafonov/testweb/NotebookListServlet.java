package ru.kpfu.agafonov.testweb;

import org.hibernate.Session;
import ru.kpfu.agafonov.testweb.models.Man;
import ru.kpfu.agafonov.testweb.models.ManFile;
import ru.kpfu.agafonov.testweb.persistence.HibernateUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;

public class NotebookListServlet extends HttpServlet {
    Session session;

    @Override
    public void init() throws ServletException {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        // hibernate
        //List<Man> people = (List<Man>) session.createCriteria(Man.class).list();

        // File storage
        List<Man> people = ManFile.getAll();

        request.setAttribute("people", people);
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/views/list.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String note = request.getParameter("note");
        Man man = new Man(name, email, phone, note);

        // hibernate
        //this.session.save(man);

        // File storage
        ManFile.add(man);

        response.sendRedirect("/");
    }

    @Override
    public void destroy() {
    }
}
