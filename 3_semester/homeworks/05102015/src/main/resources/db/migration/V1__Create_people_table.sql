CREATE TABLE people (
  id       BIGSERIAL PRIMARY KEY,
  name     VARCHAR(255),
  email    VARCHAR(255),
  phone    VARCHAR(255),
  note     TEXT
);