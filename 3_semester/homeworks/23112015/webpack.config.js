var webpack = require("webpack");
var path = require('path');

module.exports = {
    entry: "./src/app",
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: "bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loaders: ['babel?presets[]=react,presets[]=es2015'],
                exclude: /(node_modules|bower_components)/,
            },
            {
                test: /\.scss$/,
                loader: 'style!css!sass'
            }
        ]
    }
};