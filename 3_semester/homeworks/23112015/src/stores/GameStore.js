import Reflux from 'reflux'

import GameActions from '../actions/GameActions'

const LinksStore = Reflux.createStore({
    listenables: [GameActions],
    numberOfSignals: 0,
    signalFromCircle: function () {
        this.trigger(++this.numberOfSignals);
    }
});

export default LinksStore