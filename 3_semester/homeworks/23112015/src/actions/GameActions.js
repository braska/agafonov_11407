import Reflux from 'reflux';

const GameActions = Reflux.createActions([
    'signalFromCircle'
]);

export default GameActions;