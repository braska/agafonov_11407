import React from 'react';

import Circle from "./Circle";
import Config from "../config";

import GameStore from "../stores/GameStore";

const Game = React.createClass({
    getInitialState() {
        return {
            isEnded: false
        }
    },

    componentDidMount() {
        GameStore.listen(this.onChange);
    },

    onChange(numberOfEndedCircles) {
        if (numberOfEndedCircles >= 2) {
            this.setState({isEnded: true});
        }
    },

    render() {
        var circle1 = <Circle ref="circle1" numberOfBlinks={Config.numberOfBlinks} keyCode={68} />;
        var circle2 = <Circle ref="circle2" numberOfBlinks={Config.numberOfBlinks} keyCode={75} />;
        if (this.state.isEnded) {
            return (
                <div className="circle-container">
                    <h1>{this.refs.circle1.state.timeCounter > this.refs.circle2.state.timeCounter ? "Выиграл игрок 2" : (this.refs.circle1.state.timeCounter < this.refs.circle2.state.timeCounter ? "Выиграл игрок 1" : "Ничья")}</h1>
                </div>
            );
        } else {
            return (
                <div>
                    {circle1}
                    {circle2}
                </div>
            );
        }
    }
});

export default Game;