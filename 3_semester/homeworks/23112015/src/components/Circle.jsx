import React from 'react';
import RandomMC from 'random-material-color';
import GameActions from '../actions/GameActions';

const Circle = React.createClass({
    propTypes: {
        numberOfBlinks: React.PropTypes.number.isRequired,
        keyCode: React.PropTypes.number.isRequired
    },
    getInitialState() {
        return {
            count: 0,
            timeCounter: 0,
            isShown: false
        }
    },
    componentDidMount() {
        this.blink();
        window.addEventListener("keydown", this.keyDown);
    },

    componentWillUnmount: function() {
        window.removeEventListener("keydown", this.keyDown);
    },

    keyDown(event) {
        if (event.keyCode == this.props.keyCode) {
            this.touch();
        }
    },

    blink() {
        var self = this;
        if (this.state.count < this.props.numberOfBlinks) {
            if (!this.state.isShown) {
                var random = Math.floor(Math.random() * (5000 - 500 + 1)) + 500;
                setTimeout(function () {
                    self.setState({isShown: true, showTime: new Date(), count: self.state.count + 1});
                }, random);
            }
        } else {
            this.setState({count: this.state.count + 1}, function () {
                GameActions.signalFromCircle();
            });
        }
    },
    touch() {
        var self = this;
        if (this.state.isShown) {
            this.setState({
                timeCounter: this.state.timeCounter + ((new Date()) - this.state.showTime),
                isShown: false
            }, self.blink);
        } else {

        }
    },
    render() {
        if (this.state.count < this.props.numberOfBlinks + 1) {
            const style = {
                opacity: this.state.isShown ? 1 : 0,
                backgroundColor: RandomMC.getColor()
            };
            return (
                <div className="circle-container">
                    <div className="circle" style={style} onClick={this.touch}></div>
                    <p style={{textAlign: 'center'}}>{this.state.timeCounter}</p>
                </div>
            );
        } else {
            return (
                <div className="circle-container">
                    <div className="circle"><h5>Для Вас игра окончена</h5></div>
                    <p style={{textAlign: 'center'}}>{this.state.timeCounter}</p>
                </div>
            );
        }
    }
});

export default Circle;