package ru.kpfu.agafonov;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) throws IOException {
        Logger.run(Paths.get("test_loggable_dir"), new FileOutputStream("log.txt"));
    }
}
