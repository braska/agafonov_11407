package ru.kpfu.agafonov;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;
public class Logger {
    private Thread loggingThread;
    private boolean run;

    private Logger() {}

    public static Logger run(Path path, FileOutputStream output) throws IOException {
        Logger l = new Logger();
        FileSystem fileSystem = FileSystems.getDefault();
        WatchService watchService = fileSystem.newWatchService();
        path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE,StandardWatchEventKinds.ENTRY_MODIFY,
                StandardWatchEventKinds.ENTRY_DELETE);
        l.run = true;
        l.loggingThread = new Thread(() -> {
            while(l.run){
                WatchKey watchKey = null;
                try {
                    watchKey = watchService.take();
                    List<WatchEvent<?>> watchEvents = watchKey.pollEvents();
                    for (WatchEvent<?> we : watchEvents){
                        if(we.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
                            output.write(("[" + System.currentTimeMillis() + "] " + "Created: " + we.context() + "\n").getBytes());
                        }else if (we.kind() == StandardWatchEventKinds.ENTRY_DELETE) {
                            output.write(("[" + System.currentTimeMillis() + "] " + "Deleted: " + we.context() + "\n").getBytes());
                        } else if(we.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
                            output.write(("[" + System.currentTimeMillis() + "] " + "Modified: " + we.context() + "\n").getBytes());
                        }
                    }
                    if(!watchKey.reset()){
                        break;
                    }
                } catch (InterruptedException | IOException ignored) {
                    System.out.println("Что-то пошло не так");
                }
            }
        });
        l.loggingThread.start();
        return l;
    }

    public void stop() {
        this.run = false;
    }
}
