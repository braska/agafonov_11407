$(document).ready(function() {
  $('#double').on('keypress', function(e) {
    var char = String.fromCharCode(e.charCode);
    $(this).val($(this).val().substr(0, $(this).getCursorPosition()) + char + char + $(this).val().substr($(this).getCursorPosition()));
    e.preventDefault();
  });
  $('#opacity').on('focus', function() {
    $(this).css('opacity', 0);
  });
  $('#opacity').on('blur', function() {
    $(this).css('opacity', 1);
  });
  $('.linked-checkbox').on('change', function() {
    var prev = $(this).prev();
    var next = $(this).next();
    prev.prop('checked', !prev.prop('checked'));
    next.prop('checked', !next.prop('checked'));
  });
  $(window).on('copy',function(e) {
    e.preventDefault();
  });
  $('i.must_be_href').attr('href', function() {
    return $(this).attr('title');
  }).removeAttr('title').removeClass('must_be_href').replaceTag('<a>');
  $('#floating-btn').on('mouseenter', function() {
    $(this).css('position', 'fixed');
    $(this).css('top', Math.random() * ($(window).height() - $(this).height()));
    $(this).css('left', Math.random() * ($(window).width() - $(this).width()));
  });

  $(window).on('mouseup', function() {
      var selected = '';
      if (window.getSelection) {
        selected = window.getSelection().toString();
      } else if (document.selection) {
        selected = document.selection.createRange().text;
      }
      if (selected.length > 0) {
        alert("Выделено символов: " + selected.length);
      }
  });
});
