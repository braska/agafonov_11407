var factorial = function(n) {
  return n > 0 ? n * factorial(n - 1) : 1;
};

var fib = function(n) {
  var internalFib = function(n, a, b) {
    return n > 0 ? internalFib(n - 1, b, a + b) : a;
  };
  return internalFib(n,0,1);
};

var printPascalTriangle = function(n) {
  for (var i = 0; i < n; i++) {
    for (var j = 0; j < i + 1; j++) {
      document.write((factorial(i) / (factorial(j) * factorial(i - j))) + "\t");
    }
    document.write("<br>");
  }
};
