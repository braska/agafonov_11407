String.prototype.camelize = function () {
  // Находим любые символы стоящие после _.- и заменяем на их эквивалент в верхнем регистре
  return this.replace(/[_.-](\w|$)/g, function (_,x) {
    return x.toUpperCase();
  });
};
