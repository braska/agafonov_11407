package spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("customRenderer")
public class CustomMessageProvider implements MessageProvider {

    @Value("Hi, my friend!")
    private String message;

    @Override
    public String getMessage() {
        return this.message;
    }
}
