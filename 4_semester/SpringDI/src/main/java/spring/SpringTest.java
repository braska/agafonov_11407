package spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringTest {
    public static void main(String[] args) {
        ApplicationContext appContext = new ClassPathXmlApplicationContext("AppContext.xml");
        MessageRenderer mr = appContext.getBean("customRenderer", MessageRenderer.class);
        mr.render();
    }
}
