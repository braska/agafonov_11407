package spring;

public class Hello {
    public static void main(String[] args) {
        MessageRenderer messageRenderer = new SoutMessageRenderer();
        MessageProvider messageProvider = new HelloMessageProvider();
        messageRenderer.setMessageProvider(messageProvider);
        messageRenderer.render();
    }
}
