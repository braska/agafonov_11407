package spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("renderer")
public class SoutMessageRenderer implements MessageRenderer {

    @Qualifier("customRenderer")
    @Autowired
    private MessageProvider provider;

    @Override
    public void render() {
        System.out.println(provider.getMessage());
    }

    @Override
    public MessageProvider getMessageProvider() {
        return provider;
    }

    @Override
    public void setMessageProvider(MessageProvider provider) {
        this.provider = provider;
    }
}
