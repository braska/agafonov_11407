package ru.kpfu.agafonov;

public class NumberList {

    private Number first;
    private int size;

    public void add(int number) {
        // Find two numbers between which will be made to insert a new number
        Number prev = null;
        Number x = first;
        while(x != null && number > x.value) {
            prev = x;
            x = x.next;
        }
        Number n = new Number(number, null);

        // If this is the first number in the list...
        if(prev == null) {
            n.next = first;
            first = n;
        } else {
            // Otherwise, "tear" list and insert a new number
            n.next = x;
            prev.next = n;
        }
        size++;
    }

    /**
     * Reverse list
     *
     * Algorithm:
     * Move the head of list to (size - i - 1) position (size - 1) times
     */
    public void reverse() {
        for(int i = 0; i < size; i++) {
            Number n = first;

            Number prev = first;
            for (int j = 0; j < size - i - 1; j++)
                prev = prev.next;

            Number prevNext = prev.next;

            prev.next = n;
            first = n.next;

            n.next = prevNext;
        }
    }

    public int size() {
        return size;
    }

    public int get(int index) {
        Number x = first;
        for (int i = 0; i < index; i++)
            x = x.next;
        return x.value;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[");
        Number x = first;
        while (x.next != null) {
            result.append(x).append(", ");
            x = x.next;
        }
        result.append(x);
        result.append("]");
        return result.toString();
    }

    private static class Number {
        int value;
        Number next;

        Number(int value, Number next) {
            this.value = value;
            this.next = next;
        }

        @Override
        public String toString() {
            return value + "";
        }
    }
}
