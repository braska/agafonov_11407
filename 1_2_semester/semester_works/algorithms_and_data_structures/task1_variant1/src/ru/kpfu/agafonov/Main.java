package ru.kpfu.agafonov;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        if(args.length > 0) {
            String filename = args[0];
            int exitCode = 0;

            BufferedReader br = null;

            try {
                br = new BufferedReader(new FileReader(filename));
                String line = br.readLine();
                String[] numbers =  line.split(" ");
                NumberList list = new NumberList();
                for(String number : numbers) {
                    try {
                        int n = Integer.parseInt(number);
                        list.add(n);
                    } catch (NumberFormatException e) {
                        System.out.println("\"" + number + "\" is not a integer. Skipped.");
                    }
                }
                System.out.println(list);
                list.reverse();
                System.out.println(list);
            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
                exitCode = 1;
            } catch (IOException e) {
                System.out.println(e.getMessage());
                exitCode = 1;
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                        exitCode = 1;
                    }
                }
                if(exitCode != 0)
                    System.exit(exitCode);
            }
        } else {
            System.out.println("Expected filename as an argument.");
            System.exit(1);
        }
    }
}
