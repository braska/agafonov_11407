var Ship = function(cells) {
    cells.forEach(function(cell) {
        cell.ship = this;
    });

    this.isKilled = function () {
        var result = true;
        cells.forEach(function(cell) {
            result = cell.isHit();
        });
        return result;
    };

    this.isLocatedOn = function(x, y) {
        var result = false;
        cells.some(function(cell) {
            var internalResult = (cell.getX() == x && cell.getY() == y);
            if(internalResult) {
                result = internalResult;
                return true;
            }
            return false;
        });
        return result;
    };

    this.getJSON = function() {
        var result = {cells: [], isKilled: this.isKilled()};
        cells.forEach(function(cell) {
            result.cells.push(cell.getJSON());
        });
        return result;
    };
};

module.exports = Ship;