var Cell = require.main.require('./models/Cell'),
    events = require("events"),
    util = require("util"),
    Ship = require.main.require('./models/Ship');

function Field() {
    events.EventEmitter.call(this);
    var ships = [];
    var cells = [];

    var size = 10;

    // Создаём клетки, подписываем их на события удара, подписываем каждую клетку на событие убития рядомстоящего корабля.
    for (var i = 0; i < size; i++) {
        cells[i] = [];
        for (var j = 0; j < size; j++) {
            var cell = new Cell(i, j);
            this.on('shot_' + i + '_' + j, cell.shot);
            for (var ni = Math.max(0, i - 1); ni < Math.min(size, i + 1); ni++) {
                for (var nj = Math.max(0, j - 1); nj < Math.min(size, j + 1); nj++) {
                    if (!(ni == i && nj == j)) {
                        this.on('after_near_kill_ship_' + ni + '_' + nj, cell.shot);
                    }
                }
            }
            cells[i][j] = cell;
        }
    }

    this.getCells = function() {
        return cells;
    };

    this.getShips = function() {
        return ships;
    };

    this.getJSON = function() {
        var fieldObj = {cells: [], size: size, ships: []};
        this.getCells().forEach(function(row) {
            var outputRow = [];
            row.forEach(function(cell){
                outputRow.push(cell.getJSON());
            });
            fieldObj.cells.push(outputRow);
        });
        this.getShips().forEach(function(ship) {
            fieldObj.ships.push(ship.getJSON());
        });
        return fieldObj;
    };

    this.autoplaceShips = function () {
        /*var random = function () {
            return Math.floor(Math.random() * 10);
        };
        var ships = [4, 3, 3, 2, 2, 2, 1, 1, 1, 1];

        ships.forEach(function(val) {
            var x = random(),
                y = random();



            for(var i = 0; i < val; i++) {

            }
        });*/
        ships.push(new Ship([cells[0][0], cells[0][1], cells[0][2], cells[0][3]]));
        ships.push(new Ship([cells[4][0], cells[4][1], cells[4][2]]));
        ships.push(new Ship([cells[4][4], cells[4][5], cells[4][6]]));
        ships.push(new Ship([cells[0][7], cells[0][8]]));
        ships.push(new Ship([cells[4][8], cells[4][9]]));
        ships.push(new Ship([cells[2][6], cells[2][8]]));
        ships.push(new Ship([cells[0][5]]));
        ships.push(new Ship([cells[2][0]]));
        ships.push(new Ship([cells[2][2]]));
        ships.push(new Ship([cells[2][4]]));
    };

    this.autoplaceShips();
}

util.inherits(Field, events.EventEmitter);

module.exports = Field;
