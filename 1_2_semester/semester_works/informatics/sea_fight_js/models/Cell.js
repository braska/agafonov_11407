var Cell = function(x, y) {
    var isHit = false;

    this.getX = function() {
        return x;
    };

    this.getY = function() {
        return y;
    };

    this.isHit = function() {
        return isHit;
    };

    this.shot = function() {
        isHit = true;
    };

    this.isPartOfShip = function() {
        return this.ship != undefined;
    };

    this.getJSON = function() {
        return {x: this.getX(), y: this.getY(), isHit: this.isHit()}
    };
};

module.exports = Cell;