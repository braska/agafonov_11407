var config = require("./config");
var ENVIRONMENT = process.env.ENV || config.environment;
var PORT = process.env.PORT || config.port;


var redis = require('./redis-adapter'),
    io = require('socket.io')(PORT),
    ioredis = require('socket.io-redis'),
    Game = require('./controllers/Game');

io.adapter(ioredis({ pubClient: redis.client, subClient: redis.subClient }));

var findOpponentThenStartGame = function(socket) {
    redis.getWaitingPlayer(function(err, token) {
        if(!err) {
            if(token != null) {
                redis.pairPlayers(token, socket.token, function(err) {
                    if (!err) {
                        redis.session.getSocketId(token, function(err, socketId) {
                            if (!err) {
                                if (io.sockets.connected[socketId]) {
                                    new Game(socket, io.sockets.connected[socketId], redis);
                                }
                            }
                        });
                    }
                });
            } else {
                redis.addToWaitingList(socket.token, function (err) {
                    if(!err) {
                        socket.emit("wait");
                    }
                });
            }
        }
    });
};


io.on('connection', function(socket) {
    socket.on('isLoggedIn', function(callback) {
        callback(socket.token != undefined);
    });
    
    socket.on('auth', function(token, callback) {
        redis.session.isActualToken(token, function(err, resp) {
            if(!err && resp) {
                socket.token = token;
                callback(token);
                redis.session.getSocketId(token, function(err, socketId) {
                    if (!err) {
                        if (io.sockets.connected[socketId]) {
                            socket.emit("already");
                        } else {
                            redis.session.setSocketId(token, socket.id, function(err) {
                                if(!err) {
                                    redis.getOpponent(token, function(err, opponent) {
                                        if (opponent) {
                                            /*
                                            Восстанавливаем игру после разрыва
                                             */
                                        } else {
                                            findOpponentThenStartGame(socket);
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            } else {
                redis.session.create(
                    socket.id,
                    function (err, token) {
                        if (!err && token) {
                            socket.token = token;
                            callback(token);
                            findOpponentThenStartGame(socket);
                        }
                    }
                );
            }
        });
    });

    socket.on('disconnect', function () {
        redis.removeFromWaitingList(socket.token);
    });
});