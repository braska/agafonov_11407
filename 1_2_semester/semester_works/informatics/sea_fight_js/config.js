module.exports = {
    appName: "seafight",
    port: 8058,
    redis: {
        host: "127.0.0.1",
        port: 6379,
        options: {
            detect_buffers: true
        }
    },
    mongo: {
        url: "mongodb://localhost:27017/seafight"
    },
    environment: "production"
};