var config = require("./config");

var redisSubClientOptions = config.redis.options;
redisSubClientOptions.detect_buffers = true;

var redis = require("redis"),
    client = redis.createClient(config.redis.port, config.redis.host, config.redis.options),
    subClient = redis.createClient(config.redis.port, config.redis.host, redisSubClientOptions);

var generateToken = function() {
    var i, possible, t, _i;
    t = "";
    possible = "ABCDEFGHIJKLMNOPQRSTUVWXYabcdefghijklmnopqrstuvwxyz0123456789";
    for (i = _i = 0; _i < 55; i = ++_i) {
        t += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return t + 'Z' + new Date().getTime().toString(36);
};

var redisAdapter = {
    client: client,
    subClient: subClient,
    getWaitingPlayer: function(callback) {
        client.rpop(config.appName + ":awaiting", callback);
    },
    addToWaitingList: function(token, callback) {
        client.rpush(config.appName + ":awaiting", token, callback);
    },
    removeFromWaitingList: function(token, callback) {
        client.lrem(config.appName + ":awaiting", 0, token, callback);
    },
    pairPlayers: function(token1, token2, callback) {
        client.set(config.appName + ":" + token1 + ":opponent", token2, function(err, reply) {
            if (!err) {
                client.set(config.appName + ":" + token2 + ":opponent", token1, callback);
            } else
                callback(err);
        });
    },
    unpairPlayers: function(token, callback) {
        client.get(config.appName + ":" + token + ":opponent", function(err, opponent){
            if(err)
                callback(err);
            else
                if(!opponent)
                    callback();
                else
                    client.del(config.appName + ":" + token + ":opponent", function (err) {
                        if (err)
                            callback(err);
                        else
                            client.del(config.appName + ":" + opponent + ":opponent", callback);
                    });
        });
    },
    getOpponent: function (token, callback) {
        client.get(config.appName + ":" + token + ":opponent", callback);
    },
    session: {
        isActualToken: function(token, callback) {
            if(token == null || token == undefined)
                callback(null, false);
            client.sismember(config.appName + ":sessions", token, function(err, reply) {
                if(err) {
                    callback(err);
                } else
                    callback(null, reply == true);
            });
        },
        create: function(socketId, callback) {
            var token = generateToken();
            client.sadd(config.appName + ":sessions", token, function (err, reply) {
                if(err || !reply)
                    callback(err, false);
                else {
                    redisAdapter.session.setSocketId(token, socketId, function(err) {
                        if(err) {
                            callback(err, false);
                        } else {
                            callback(null, token);
                        }
                    });
                }
            });
        },
        delete: function(token, callback) {
            client.srem(config.appName + ":sessions", token, function(err, reply) {
                if(err)
                    callback(err);
                else {
                    redisAdapter.removeFromWaitingList(token, function (err) {
                        if(err) {
                            callback(err);
                        } else {
                            redisAdapter.unpairPlayers(token, function(err) {
                                if (err)
                                    callback(err);
                                else
                                    redisAdapter.session.deleteData(token, function(err) {
                                        if (err) {
                                            callback(err);
                                        } else {
                                            redisAdapter.session.deleteSocketId(token, callback);
                                        }
                                    });
                            });
                        }
                    });
                }
            });
        },
        getData: function(token, callback) {
            client.get(config.appName + ":sessions:data:" + token, function(err, reply) {
                if(err || !reply)
                    callback(err, null);
                else {
                    callback(null, JSON.parse(reply));
                }
            });
        },
        deleteData: function(token, callback) {
            client.del(config.appName + ":sessions:data:" + token, callback);
        },
        getSocketId: function (token, callback) {
            client.get(config.appName + ":sessions:socketId:" + token, callback);
        },
        setData: function(token, data, callback) {
            client.set(config.appName + ":sessions:data:" + token, JSON.stringify(data), callback);
        },
        setSocketId: function(token, socketId, callback) {
            client.set(config.appName + ":sessions:socketId:" + token, socketId, callback);
        },
        deleteSocketId: function(token, callback) {
            client.del(config.appName + ":sessions:socketId:" + token, callback);
        }
    }
};

module.exports = redisAdapter;

