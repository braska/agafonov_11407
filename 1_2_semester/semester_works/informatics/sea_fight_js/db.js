var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

module.exports = function (config) {
    return {
        insert: function(col, data, callback) {
            MongoClient.connect(config.url, function(err, db) {
                assert.equal(null, err);
                var collection = db.collection(col);
                collection.insertOne(data, function(err, result) {
                    assert.equal(err, null);
                    callback(result);
                    db.close();
                });
            });
        },
        find: function(col, data, callback) {
            MongoClient.connect(config.url, function(err, db) {
                assert.equal(null, err);
                var collection = db.collection(col);
                collection.findOne(data, function(err, result) {
                    assert.equal(err, null);
                    callback(result);
                    db.close();
                });
            });
        }
    };
};