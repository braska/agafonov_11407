define(function () {
    return {
        getDomainName: function () {
            var hostName = window.location.host;
            return hostName.substring(hostName.lastIndexOf(".", hostName.lastIndexOf(".") - 1) + 1);
        }
    };
});