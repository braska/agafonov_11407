require(['./functions'], function(functions) {
    requirejs.config({
        baseUrl: 'js',
        paths: {
            jquery: 'jquery-2.1.3.min',
            bootstrap: 'bootstrap.min',
            socketio: ['http://io.' + functions.getDomainName() + '/socket.io/socket.io','socket.io'],
            backbone: 'backbone-min',
            underscore: 'underscore-min',
            functions: 'functions',
            views: './views',
            cookie: 'jquery.cookie-1.4.1.min'
        },
        shim: {
            backbone: {
                deps: ['jquery', 'underscore'],
                exports: 'Backbone'
            },
            underscore: {
                exports: '_'
            },
            bootstrap : {
                deps: ['jquery']
            }
        }
    });
    require(['./index']);
});