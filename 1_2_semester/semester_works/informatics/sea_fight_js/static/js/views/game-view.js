var app = app || {};

define(['backbone', 'underscore', 'jquery', 'cookie'], function (Backbone, _, $) {
    return Backbone.View.extend({

        el: '#content',

        loading: _.template($('#loading').html()),
        connectionError: _.template($('#connectionError').html()),
        gameTemplate: _.template($('#gameTemplate').html()),
        waitOpponent: _.template($('#waitOpponent').html()),
        already: _.template($('#already').html()),
        isGameComing: false,
        field: {size: 10, cells: []},

        initialize: function () {
            this.$messages = this.$el.siblings('#messages');
            var self = this;
            app.socket.on('message', function(message) {
                var type = message.type || "danger";
                self.$messages.html('<div class="alert alert-' + type + '">' + message.text + '</div>');
            });
            app.socket.on('wait', function() {
                self.$el.html(self.waitOpponent());
            });
            app.socket.on('already', function() {
                self.$el.html(self.already());
            });
            app.socket.on('updateField', function(field) {

            });
            app.socket.on('initializeGame', function(field) {
                self.field = field;
                console.log(field);
                self.isGameComing = true;
                self.render();
                app.socket.emit('gameInitialized');
            });
        },

        render: function() {
            this.$el.html(this.loading());
            var self = this;
            if (app.connect_error_alert) {
                this.$el.html(this.connectionError());
            } else if (this.isGameComing) {
                this.$el.html(this.gameTemplate());
            }
        }
    });
});