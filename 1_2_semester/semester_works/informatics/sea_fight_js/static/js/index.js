var app = app || {};

define(['jquery', 'socketio', 'functions', 'views/game-view', 'cookie'], function ($, io, functions, gameView) {
    $(function () {
        app.domain = functions.getDomainName();

        app.token = $.cookie("token");
        app.socket = io.connect('http://io.' + app.domain);


        app.gameView = new gameView();
        app.connect_error_alert = false;
        app.socket.io.on("connect_error", function (err) {
            app.connect_error_alert = true;
            app.gameView.render();
        });

        app.socket.io.on("reconnect", function () {
            app.connect_error_alert = false;
        });

        app.socket.on("connect", function() {
            app.socket.emit('auth', app.token, function(token) {
                $.cookie('token', token);
                app.gameView.render();
            });
        });
    });
});