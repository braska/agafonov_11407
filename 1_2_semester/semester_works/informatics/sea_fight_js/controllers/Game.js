var Field = require.main.require('./models/Field');

var Game = function(player1, player2, redis) {
    var player1field = new Field();
    var player2field = new Field();

    redis.session.setData(player1.token, {field: player1field.getJSON()});
    redis.session.setData(player2.token, {field: player2field.getJSON()});

    this.sendFieldToClient = function() {
        socket.emit('updateField', field.getJSON());
    };

    //socket.on('gameInitialized', this.sendFieldToClient);
    player1.emit('initializeGame', player1field.getJSON());
    player2.emit('initializeGame', player2field.getJSON());
};

module.exports = Game;
