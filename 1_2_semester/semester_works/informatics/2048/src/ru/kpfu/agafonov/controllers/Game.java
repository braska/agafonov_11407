package ru.kpfu.agafonov.controllers;

import ru.kpfu.agafonov.interfaces.Help;
import ru.kpfu.agafonov.interfaces.StorageManager;
import ru.kpfu.agafonov.models.*;

import static ru.kpfu.agafonov.models.Direction.*;
import ru.kpfu.agafonov.interfaces.Listener;
import ru.kpfu.agafonov.interfaces.InputManager;
import ru.kpfu.agafonov.views.console.ConsoleHelp;

import java.util.ArrayList;
import java.util.HashMap;

public class Game implements Listener {
    private int size, score;
    private boolean won, over;
    private Grid grid;
    private static final int START_TILES = 2;
    private InputManager inputManager = new ConsoleInputManager(this);
    private StorageManager storageManager = new FileStorageManager();
    private Help help = new ConsoleHelp();

    public Game(int size) {
        this.size = size;
        setup();
    }

    /**
     * Set storage, view, input controllers.
     * Start cycle in which we have drawn field and are waiting for input then check the completeness of the game.
     */
    public void start() {
        storageManager.setGrid(grid);
        ViewController viewController = new ViewController();
        Integer bestScore = storageManager.getBestScore();
        while (!isGameTerminated()) {
            bestScore = storageManager.getBestScore();
            viewController.render(grid, score, bestScore == null ? 0 : bestScore, won, over);
            inputManager.waitEvent();

            if(bestScore == null || score > bestScore) {
                bestScore = score;
                storageManager.setBestScore(score);
            }
            storageManager.setScore(score);
            storageManager.setGrid(grid);
        }
        storageManager.clearGameState();
        viewController.render(grid, score, bestScore == null ? 0 : bestScore, won, over);
    }

    /**
     * Restart game
     * Erase field and clear score
     */
    private void restart() {
        storageManager.clearGameState();
        setup();
    }

    /**
     * Initialize game field and score (from storage if it is not empty)
     */
    private void setup() {
        Grid prevGrid = storageManager.getGrid();
        Integer prevScore = storageManager.getScore();
        grid        = prevGrid == null ? new Grid(this.size) : prevGrid;
        score       = prevScore == null ? 0 : prevScore;
        over        = false;
        won         = false;

        if(prevGrid == null)
            addStartTiles();
    }

    /**
     * Add {START_TILES} tiles to random positions
     */
    private void addStartTiles() {
        for (int i = 0; i < START_TILES; i++) {
            addRandomTile();
        }
    }

    /**
     * Add tile to not empty cell
     */
    private void addRandomTile() {
        if (grid.haveEmptyCell()) {
            Tile tile = new Tile();
            grid.insertTile(tile);
        }
    }

    /**
     * Game finished
     * @return if game finished return true, else false
     */
    public boolean isGameTerminated() {
        return over || won;
    }

    /**
     * Call methods depending on the event
     * @param event Event
     */
    public void on(Event event) {
        switch (event) {
            case UP:
                move(UP);
                break;
            case DOWN:
                move(DOWN);
                break;
            case LEFT:
                move(LEFT);
                break;
            case RIGHT:
                move(RIGHT);
                break;
            case HELP:
                help.show();
                break;
            case EXIT:
                System.exit(0);
                break;
            case RESTART:
                restart();
                break;
        }
    }

    /**
     * Move all allowed tiles in given direction
     * @param direction Direction
     */
    private void move(Direction direction) {
        Tile[][] cells = grid.getCells();
        HashMap<Asix, ArrayList<Integer>> path = buildPath(direction.getVector());

        boolean moved = false;

        for(Tile[] cols : cells) {
            for (Tile tile : cols) {
                if(tile != null)
                    tile.merged = false;
            }
        }

        for(int x : path.get(Asix.X)) {
            for(int y : path.get(Asix.Y)) {
                Tile tile = cells[x][y];
                Position oldPosition = new Position(x, y);

                if(tile != null) {
                    Position newPosition = findNewPosition(oldPosition, direction.getVector());
                    Tile next = grid.getCell(newPosition);

                    if (next != null && next.getValue() == tile.getValue() && !next.merged) {
                        Tile merged = new Tile(tile.getValue() * 2);
                        merged.merged = true;

                        grid.insertTile(newPosition, merged);
                        grid.eraseCell(oldPosition);

                        score += merged.getValue();

                        if (merged.getValue() == 2048) won = true;
                    } else {
                        newPosition = new Position(newPosition.x - direction.getVector().x, newPosition.y - direction.getVector().y);
                        grid.eraseCell(oldPosition);
                        grid.insertTile(newPosition, tile);
                    }


                    if (!oldPosition.equals(newPosition)) {
                        moved = true;
                    }
                }
            }
        }

        if (moved) {
            addRandomTile();
            if (!movesAvailable())
                over = true;
        }
    }

    /**
     * Check the availability of movement
     * @return true if we can move at least one, else false
     */
    private boolean movesAvailable() {
        return grid.haveEmptyCell() || tileMatchesAvailable();
    }

    /**
     * Check tile matches
     * @return true if the movement of at least one tile in any of 4 directions, after which the "concatination" will be done, is available; else return false
     */
    private boolean tileMatchesAvailable() {
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                Tile tile = grid.getCell(new Position(x, y));

                if(tile != null) {
                    for(Direction direction : Direction.values()) {
                        Position potentialPosition = new Position(x + direction.getVector().x, y + direction.getVector().y);
                        Tile other = grid.getCell(potentialPosition);

                        if(other != null && other.getValue() == tile.getValue()) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     *
     * @param vector
     * @return set of positions
     */
    private HashMap<Asix, ArrayList<Integer>> buildPath(Position vector) {
        HashMap<Asix, ArrayList<Integer>> path =  new HashMap<>();
        ArrayList<Integer> x = new ArrayList<>();
        ArrayList<Integer> y = new ArrayList<>();

        for (int pos = 0; pos < size; pos++) {
            if (vector.x == 1) x.add(size - 1 - pos); else x.add(pos);
            if (vector.y == 1) y.add(size - 1 - pos); else y.add(pos);
        }

        path.put(Asix.X, x);
        path.put(Asix.Y, y);
        return path;
    }

    private Position findNewPosition(Position position, Position vector) {
        do {
            position = new Position(position.x + vector.x, position.y + vector.y);
        } while (grid.withinBounds(position) && this.grid.isEmptyCell(position));
        return position;
    }
}
