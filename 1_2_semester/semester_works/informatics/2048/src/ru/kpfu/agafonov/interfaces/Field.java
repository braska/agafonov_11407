package ru.kpfu.agafonov.interfaces;

import ru.kpfu.agafonov.models.Grid;

public interface Field {
    public void update(Grid grid);
}
