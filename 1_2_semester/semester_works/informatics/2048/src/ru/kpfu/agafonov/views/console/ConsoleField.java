package ru.kpfu.agafonov.views.console;

import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;
import ru.kpfu.agafonov.interfaces.Field;
import ru.kpfu.agafonov.models.Grid;
import ru.kpfu.agafonov.models.Tile;

import static org.fusesource.jansi.Ansi.ansi;

public class ConsoleField implements Field {
    public void update(Grid grid) {
        Tile[][] cells = grid.getCells();
        int size = grid.getSize();
        AnsiConsole.systemInstall();

        for(int y = 0; y < size; y++) {
            for(int x = 0; x < size; x++) {
                Tile tile = cells[x][y];
                if(tile != null) {
                    Ansi.Color color;
                    int pow = (int)(Math.log(tile.getValue()) / Math.log(2));
                    if(pow <= 7)
                        color = Ansi.Color.values()[8 - pow];
                    else
                        color = Ansi.Color.WHITE;
                    StringBuilder str = new StringBuilder();
                    str.append(ansi().bg(Ansi.Color.WHITE).fg(Ansi.Color.BLACK).a("|").reset());
                    for(int i = 0; i < (4 - (tile.getValue() + "").length()); i++)
                        str.append(ansi().bg(color).a(" ").reset());
                    str.append(ansi().bg(color).fg(color == Ansi.Color.WHITE ? Ansi.Color.BLACK : Ansi.Color.WHITE).bold().a(tile.getValue()).boldOff().reset());
                    str.append(ansi().bg(Ansi.Color.WHITE).fg(Ansi.Color.BLACK).a("|").reset());
                    System.out.print(str);
                } else {
                    System.out.print(ansi().bg(Ansi.Color.WHITE).fg(Ansi.Color.BLACK).a("|    |").reset());
                }
            }
            System.out.println();
            for(int x2 = 0; x2 < size; x2++) {
                System.out.print(ansi().bg(Ansi.Color.WHITE).fg(Ansi.Color.BLACK).a("------").reset());
            }
            System.out.println();
        }
        AnsiConsole.systemUninstall();
    }
}
