package ru.kpfu.agafonov.interfaces;

import ru.kpfu.agafonov.models.Grid;

public interface Layout {
    public void update(Grid grid, int score, int bestScore, boolean won, boolean over);
}
