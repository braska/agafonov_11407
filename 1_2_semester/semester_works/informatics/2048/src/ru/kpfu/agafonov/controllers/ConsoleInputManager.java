package ru.kpfu.agafonov.controllers;

import jline.ConsoleReader;
import ru.kpfu.agafonov.interfaces.Listener;
import ru.kpfu.agafonov.interfaces.InputManager;
import ru.kpfu.agafonov.models.Event;

import java.io.IOException;

public class ConsoleInputManager implements InputManager {

    Listener callback;

    public ConsoleInputManager(Listener e) {
        callback = e;
    }

    public void waitEvent() {
        System.out.println("Press up/W, right/D, down/S, left/A, H (help), Esc (exit) or R (restart): ");
        try {
            ConsoleReader reader = new ConsoleReader();
            int k = reader.readVirtualKey();
            switch (k) {
                case 16:
                case 119:
                    callback.on(Event.UP);
                    break;
                case 6:
                case 100:
                    callback.on(Event.RIGHT);
                    break;
                case 14:
                case 115:
                    callback.on(Event.DOWN);
                    break;
                case 2:
                case 97:
                    callback.on(Event.LEFT);
                    break;
                case 27:
                    callback.on(Event.EXIT);
                    break;
                case 114:
                    callback.on(Event.RESTART);
                    break;
                case 104:
                    callback.on(Event.HELP);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
