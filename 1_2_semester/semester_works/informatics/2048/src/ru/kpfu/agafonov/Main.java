package ru.kpfu.agafonov;

import ru.kpfu.agafonov.controllers.Game;
import ru.kpfu.agafonov.views.console.ConsoleHelp;

public class Main {

    public static Game game;

    public static void main(String[] args) {
        for (String arg : args) {
            if (arg.equals("--help") || arg.equals("-h")) {
                ConsoleHelp help = new ConsoleHelp();
                help.show();
                System.exit(0);
            }
        }

        game = new Game(4);
        game.start();
    }
}
