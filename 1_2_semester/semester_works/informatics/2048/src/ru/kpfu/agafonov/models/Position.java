package ru.kpfu.agafonov.models;

public class Position {
    public final Integer x, y;

    public Position(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public boolean equals(Position other) {
        return x.equals(other.x) && y.equals(other.y);
    }
}
