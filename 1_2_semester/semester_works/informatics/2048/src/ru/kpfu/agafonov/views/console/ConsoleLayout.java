package ru.kpfu.agafonov.views.console;

import org.fusesource.jansi.AnsiConsole;
import ru.kpfu.agafonov.interfaces.Field;
import ru.kpfu.agafonov.interfaces.Layout;
import ru.kpfu.agafonov.models.Grid;

import static org.fusesource.jansi.Ansi.Color.GREEN;
import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.ansi;

public class ConsoleLayout implements Layout {
    private Field field;

    public ConsoleLayout(Field field) {
        this.field = field;
    }

    public void update(Grid grid, int score, int bestScore, boolean won, boolean over) {
        AnsiConsole.systemInstall();
        System.out.print(ansi().eraseScreen());
        System.out.print(ansi().cursor(1, 1));
        System.out.print("Best score: " + bestScore + "\t");
        System.out.print("Score: " + score);
        System.out.println();
        field.update(grid);
        if(won) {
            System.out.println(ansi().fg(GREEN).a("You won!").reset());
        } else if(over) {
            System.out.println(ansi().fg(RED).a("You lose :(").reset());
        }
        AnsiConsole.systemUninstall();
    }
}
