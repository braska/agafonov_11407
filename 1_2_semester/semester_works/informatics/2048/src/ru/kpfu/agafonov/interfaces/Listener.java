package ru.kpfu.agafonov.interfaces;

import ru.kpfu.agafonov.models.Event;

public interface Listener {
    public void on(Event eventName);
}
