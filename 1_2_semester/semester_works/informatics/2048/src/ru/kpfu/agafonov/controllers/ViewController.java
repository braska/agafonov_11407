package ru.kpfu.agafonov.controllers;

import ru.kpfu.agafonov.interfaces.Field;
import ru.kpfu.agafonov.interfaces.Layout;
import ru.kpfu.agafonov.models.Grid;
import ru.kpfu.agafonov.views.console.ConsoleField;
import ru.kpfu.agafonov.views.console.ConsoleLayout;

public class ViewController {
    private Field field = new ConsoleField();
    private Layout layout = new ConsoleLayout(field);
    public void render(Grid grid, int score, int bestScore, boolean won, boolean over) {
        layout.update(grid, score, bestScore, won, over);
    }
}
