package ru.kpfu.agafonov.interfaces;

import ru.kpfu.agafonov.models.Grid;

public interface StorageManager {
    public void setBestScore(int score);

    public Integer getBestScore();

    public void setScore(int score);

    public Integer getScore();

    public void setGrid(Grid grid);

    public Grid getGrid();

    public void clearGameState();
}
