package ru.kpfu.agafonov.models;

public enum Event {
    UP,
    RIGHT,
    DOWN,
    LEFT,
    EXIT,
    HELP,
    RESTART
}
