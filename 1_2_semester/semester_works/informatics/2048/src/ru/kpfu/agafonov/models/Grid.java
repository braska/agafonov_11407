package ru.kpfu.agafonov.models;

import java.util.ArrayList;
import java.util.Random;

public class Grid {
    private int size;
    private Tile[][] cells;

    public Grid(int size) {
        if(size > 0) {
            this.size = size;
        } else {
            throw new IllegalArgumentException();
        }

        cells = new Tile[size][size];
    }

    public ArrayList<Position> emptyPositions() {
        ArrayList<Position> cells = new ArrayList<>();

        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                Position position = new Position(x, y);
                if(isEmptyCell(position))
                    cells.add(new Position(x, y));
            }
        }

        return cells;
    }

    public boolean haveEmptyCell() {
        return emptyPositions().size() > 0;
    }

    public boolean isEmptyCell(Position position) {
        return !isOccupiedCell(position);
    }

    public boolean isOccupiedCell(Position position) {
        return getCell(position) != null;
    }

    public Position getRandomEmptyPosition() {
        ArrayList<Position> emptyPositions = emptyPositions();
        Random randomizer = new Random();
        int random = randomizer.nextInt(emptyPositions.size());
        return emptyPositions.get(random);
    }

    public void insertTile(Tile tile) {
        Position position = getRandomEmptyPosition();
        insertTile(position, tile);
    }

    public void insertTile(Position position, Tile tile) {
        cells[position.x][position.y] = tile;
    }

    public void eraseCell(Position position) {
        cells[position.x][position.y] = null;
    }

    public Tile getCell(Position position) {
        if(withinBounds(position))
            return cells[position.x][position.y];
        else
            return null;
    }

    public Tile[][] getCells() {
        return cells;
    }

    public int getSize() {
        return size;
    }

    public boolean withinBounds(Position position) {
        return position.x >= 0 && position.x < size && position.y >= 0 && position.y < size;
    }
}
