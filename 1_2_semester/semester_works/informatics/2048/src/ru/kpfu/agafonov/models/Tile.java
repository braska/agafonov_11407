package ru.kpfu.agafonov.models;

import java.util.Random;

public class Tile {
    private int value;
    public boolean merged = false;

    public Tile(int value) {
        /* check whether a value is a power of two */
        if((value != 0) && ((value & (value-1)) == 0)) {
            this.value = value;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Generate tile with random value (2 or 4)
     */
    public Tile() {
        this(new Random().nextInt(4) == 3 ? 4 : 2);
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value + "";
    }
}
