package ru.kpfu.agafonov.views.console;

import jline.ConsoleReader;
import org.fusesource.jansi.AnsiConsole;
import ru.kpfu.agafonov.interfaces.Help;

import java.io.IOException;

import static org.fusesource.jansi.Ansi.ansi;

public class ConsoleHelp implements Help {
    public void show() {
        AnsiConsole.systemInstall();
        System.out.println(ansi().eraseScreen());
        System.out.println(ansi().cursor(1, 1));
        AnsiConsole.systemUninstall();

        System.out.println("Available options: ");
        System.out.println("-h (--help)\tShow this message.");
        System.out.println();
        System.out.println("In game commands: ");
        System.out.println("W or up arrow\t\tMove all tiles up.");
        System.out.println("S or down arrow\t\tMove all tiles down.");
        System.out.println("A or left arrow\t\tMove all tiles left.");
        System.out.println("D or right arrow\tMove all tiles right.");
        System.out.println("R\t\t\tRestart game.");
        System.out.println("H\t\t\tShow this message.");
        System.out.println("Esc or CTRL+C\t\tLeave game.");
        System.out.println();
        try {
            ConsoleReader reader = new ConsoleReader();
            System.out.println("Press any key for continue...");
            reader.readVirtualKey();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
