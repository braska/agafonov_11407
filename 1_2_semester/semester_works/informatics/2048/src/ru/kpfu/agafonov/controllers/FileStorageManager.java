package ru.kpfu.agafonov.controllers;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ru.kpfu.agafonov.interfaces.StorageManager;
import ru.kpfu.agafonov.models.Grid;
import ru.kpfu.agafonov.models.Position;
import ru.kpfu.agafonov.models.Tile;

import java.io.*;

public class FileStorageManager implements StorageManager {
    private static final String FILE_NAME = "2048.save";
    private Grid grid;
    private Integer bestScore, score;

    public FileStorageManager() {
        load();
    }

    public void setBestScore(int score) {
        bestScore = score;
        save();
    }

    public Integer getBestScore() {
        load();
        return bestScore;
    }

    public void setScore(int score) {
        this.score = score;
        save();
    }

    public Integer getScore() {
        load();
        return score;
    }


    public void setGrid(Grid grid) {
        this.grid = grid;
        save();
    }

    public Grid getGrid() {
        load();
        return grid;
    }

    private void load() {
        try (BufferedReader file = new BufferedReader(new FileReader(FILE_NAME))) {
            String json = file.readLine();
            JSONParser parser = new JSONParser();
            JSONObject object = (JSONObject) parser.parse(json);
            Long longBestScore = (Long) object.get("bestScore");
            if(longBestScore != null)
                bestScore = longBestScore.intValue();
            Long longScore = (Long) object.get("score");
            if(longScore != null)
                score = longScore.intValue();

            JSONObject grid = (JSONObject) object.get("grid");
            if(grid != null) {
                Integer gridSize = ((Long) grid.get("size")).intValue();
                Grid gridObject = new Grid(gridSize);
                JSONArray gridCols = (JSONArray) grid.get("content");
                for (int i = 0; i < gridSize; i++) {
                    JSONArray gridCol = (JSONArray) gridCols.get(i);
                    for (int j = 0; j < gridSize; j++) {
                        Integer value = ((Long) gridCol.get(j)).intValue();
                        if(value != 0)
                            gridObject.insertTile(new Position(i, j), new Tile(value));
                    }
                }
                this.grid = gridObject;
            }
        } catch (IOException e) {
            System.err.println("Can't load save file");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void save() {
        JSONObject game = new JSONObject();
        if(bestScore != null)
            game.put("bestScore", bestScore);
        if(score != null)
            game.put("score", score);

        if(this.grid != null) {
            JSONObject grid = new JSONObject();
            grid.put("size", this.grid.getSize());
            JSONArray cols = new JSONArray();

            for (Tile[] col : this.grid.getCells()) {
                JSONArray jsonCol = new JSONArray();
                for (Tile tile : col) {
                    if(tile != null)
                        jsonCol.add(tile.getValue());
                    else
                        jsonCol.add(0);
                }
                cols.add(jsonCol);
            }
            grid.put("content", cols);
            game.put("grid", grid);
        }

        try(FileWriter file = new FileWriter(FILE_NAME)) {
            file.write(game.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clearGameState() {
        grid = null;
        score = null;
        save();
    }
}
