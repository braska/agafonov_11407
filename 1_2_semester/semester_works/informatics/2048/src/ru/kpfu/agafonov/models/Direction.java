package ru.kpfu.agafonov.models;

public enum Direction {
    UP {
        public Position getVector() { return new Position(0, -1); }
    },
    RIGHT {
        public Position getVector() { return new Position(1, 0); }
    },
    DOWN {
        public Position getVector() { return new Position(0, 1); }
    },
    LEFT {
        public Position getVector() { return new Position(-1, 0); }
    };

    public abstract Position getVector();
}
