package ru.kpfu.agafonov.models;

public interface ShotResultPromise {
    void resolve(ShotResult result);
}
