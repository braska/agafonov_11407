package ru.kpfu.agafonov.models;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class Ship {
    private Set<Cell> cells = new HashSet<>();

    /**
     * Create ship from set of cell models
     * @param cells set of cell models
     */
    public Ship(Set<Cell> cells) {
        this.cells.addAll(cells);
        cells.forEach(cell -> cell.ship = this);
    }

    /**
     * Create ship from cell array
     * @param cells cells
     */
    public Ship(Cell... cells) {
        Collections.addAll(this.cells, cells);
        for (Cell cell : cells) {
            cell.ship = this;
        }
    }

    /**
     * Check whether ship is killed
     * @return
     */
    public boolean isKilled() {
        return cells.stream().filter(cell -> !cell.isHit()).count() == 0;
    }

    /**
     * Check field location
     * @param x X-axis coord
     * @param y Y-axis coord
     * @return true if located else false
     */
    public boolean isLocatedOn(int x, int y) {
        return cells.stream().filter(obj -> obj.getX() == x && obj.getY() == y).findFirst().isPresent();
    }

    /**
     * Add deck to ship
     * @param deck cell model
     */
    public void addDeck(Cell deck) {
        cells.add(deck);
        deck.ship = this;
    }

    /**
     * Get set of all cell models from ship
     * @return set of all cell models
     */
    public Set<Cell> getCells() {
        return cells;
    }
}
