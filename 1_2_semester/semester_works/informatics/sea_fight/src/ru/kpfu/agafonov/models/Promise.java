package ru.kpfu.agafonov.models;

public interface Promise {
    void resolve();
}
