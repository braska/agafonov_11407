package ru.kpfu.agafonov.services;

import ru.kpfu.agafonov.models.*;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GameTransport {
    private SocketInput in;
    private SocketOutput out;

    public GameTransport(SocketInput in, SocketOutput out) {
        this.in = in;
        this.out = out;
    }

    /**
     * Send shot coords and wait response
     * @param x X-axis coord
     * @param y Y-axis coord
     * @param onResult callback (It will be called after the return of the result)
     * @throws IOException
     */
    public void shot(int x, int y, ShotResultPromise onResult) throws IOException {
        System.out.println("Бьём по " + x + "," + y);
        in.waitResponse(1, response -> {
            System.out.println("Ответ: " + response);
            ShotResult result = ShotResult.fromString(response);
            if (result != null) {
                onResult.resolve(result);
            } else {
                throw new ResponseException();
            }
        });
        out.send(x + "," + y);
    }

    /**
     * Receive shot coords
     * @param onShot callback
     */
    public void waitShot(ShotPromise onShot) {
        in.waitResponse(3, response -> {
            System.out.println("Нам бьют по: " + response);
            Pattern pattern = Pattern.compile("([0-9]+),([0-9]+)");
            Matcher matcher = pattern.matcher(response);
            if (matcher.matches()) {
                int x = Integer.parseInt(matcher.group(1));
                int y = Integer.parseInt(matcher.group(2));
                onShot.resolve(x, y);
            } else {
                throw new ResponseException();
            }
        });
    }

    /**
     * Wait opponent ready
     * @param onReady callback
     */
    public void waitOpponentReady(Promise onReady) {
        in.waitResponse(1, response -> {
            System.out.println("Оппонент готов: " + response);
            if (response.equals("1")) {
                onReady.resolve();
            } else {
                throw new ResponseException();
            }
        });
    }

    /**
     * Send myself ready status to opponent
     * @throws IOException
     */
    public void sendIAmReady() throws IOException {
        System.out.println("Отсылаем информацию о готовности");
        out.send("1");
    }

    /**
     * Send the result in response to the shot
     * @param result shot result
     * @throws IOException
     */
    public void sendShotResult(ShotResult result) throws IOException {
        System.out.println("Отсылаем " + result.getValue());
        out.send(result.getValue() + "");
    }

    public class ResponseException extends RuntimeException {
        public ResponseException() {
            super();
        }
    }
}
