package ru.kpfu.agafonov.models;

public enum Orientation {
    HORIZONTAL,
    VERTICAL
}
