package ru.kpfu.agafonov.models;

public interface ShotPromise {
    void resolve(int x, int y);
}
