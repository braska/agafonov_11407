package ru.kpfu.agafonov.views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class GameOver extends JPanel {
    public GameOver(boolean win) {
        JLabel message = new JLabel();
        message.setBorder(new EmptyBorder(30, 60, 30, 60));
        message.setFont(new Font("Serif", Font.PLAIN, 50));
        message.setForeground(win ? new Color(0, 128, 0) : Color.RED);
        message.setText(win ? "Вы выиграли" : "Вы проиграли");
        this.add(message, BorderLayout.CENTER);
    }
}
