package ru.kpfu.agafonov.services;

import java.io.IOException;
import java.net.*;

public class SocketOutput {

    protected DatagramSocket outSocket;

    public SocketOutput(SocketAddress outputAddress) throws IOException {
        this(outputAddress, true);
    }

    /**
     * Create socket, connect to opponent, send welcome message
     * @param outputAddress opponent address
     * @param autoHi send welcome message automatically
     * @throws IOException
     */
    public SocketOutput(SocketAddress outputAddress, boolean autoHi) throws IOException {
        outSocket = new DatagramSocket();
        outSocket.connect(outputAddress);
        if (autoHi)
            send("hi");
    }

    /**
     * Send some message
     * @param message message content
     * @throws IOException
     */
    public void send(String message) throws IOException {
        byte[] messageAsByteArr = message.getBytes();
        outSocket.send(new DatagramPacket(messageAsByteArr, messageAsByteArr.length));
    }

    /**
     * Close socket
     */
    public void stop(){
        this.outSocket.close();
    }
}
