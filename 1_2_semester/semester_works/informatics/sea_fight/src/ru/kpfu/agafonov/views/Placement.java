package ru.kpfu.agafonov.views;

import ru.kpfu.agafonov.models.*;

import javax.swing.*;
import java.awt.*;
import java.util.HashSet;
import java.util.Set;

public class Placement extends JPanel {

    private Set<Integer> placed = new HashSet<>();
    private Integer currentShip;
    private Integer[] ships;
    private Box shipsBox = Box.createVerticalBox();
    private Orientation orientation = Orientation.VERTICAL;
    private Set<Cell> targets = new HashSet<>();



    public Placement(ru.kpfu.agafonov.models.Field myFieldModel, Promise onReady, Integer... ships) {
        this.ships = ships;
        Field myField = new Field(myFieldModel);
        myField.addPlacementListener(
                cellBtn -> {
                    // click
                    if (targets.size() > 0) {
                        Set<ru.kpfu.agafonov.models.Cell> targetsModels = new HashSet<>();
                        targets.forEach(target -> targetsModels.add(target.getCellModel()));
                        myFieldModel.addShip(new Ship(targetsModels));
                        clearTargets();
                        placed.add(currentShip);
                        currentShip = null;
                        renderShipsBox();
                        myField.rerender();
                        if (ships.length == placed.size()) {
                            onReady.resolve();
                        }
                    }
                },
                cellBtn -> {
                    // mouse enter
                    ru.kpfu.agafonov.models.Cell cell = cellBtn.getCellModel();
                    if (currentShip != null) {
                        boolean placeContainsShip = false;
                        boolean placeIsInOfFieldBounds = (orientation == Orientation.HORIZONTAL && ((cell.getY() + ships[currentShip]) <= myFieldModel.getSize())) || (orientation == Orientation.VERTICAL && ((cell.getX() + ships[currentShip]) <= myFieldModel.getSize()));
                        if (placeIsInOfFieldBounds)
                            deckChecker: for (int i = 0; i < ships[currentShip]; i++) {
                                int x, y;
                                if (orientation == Orientation.HORIZONTAL) {
                                    x = cell.getX();
                                    y = cell.getY() + i;
                                } else {
                                    x = cell.getX() + i;
                                    y = cell.getY();
                                }

                                for (int ni = Math.max(0, x - 1); ni <= Math.min(myFieldModel.getSize() - 1, x + 1); ni++) {
                                    for (int nj = Math.max(0, y - 1); nj <= Math.min(myFieldModel.getSize() - 1, y + 1); nj++) {
                                        Cell target = myField.getCell(ni, nj);
                                        if (target.getCellModel().isPartOfShip()) {
                                            placeContainsShip = true;
                                            clearTargets();
                                            break deckChecker;
                                        }
                                    }
                                }

                                Cell target = myField.getCell(x, y);

                                targets.add(target);
                            }
                        if (!placeContainsShip && placeIsInOfFieldBounds) {
                            targets.forEach(target -> target.setBackground(Color.ORANGE));
                        }
                    }
                },
                cellBtn -> {
                    // mouse leave
                    clearTargets();
                }
        );

        this.add(myField);
        this.add(Box.createVerticalStrut(30));
        renderShipsBox();
        this.add(shipsBox);
        this.add(Box.createVerticalStrut(30));
        JButton changeOrientation = new JButton("Повернуть");
        changeOrientation.addActionListener(e -> {
            if (orientation == Orientation.VERTICAL)
                orientation = Orientation.HORIZONTAL;
            else
                orientation = Orientation.VERTICAL;
        });
        this.add(changeOrientation);
    }

    private void renderShipsBox() {
        shipsBox.removeAll();
        for (int i = 0; i < ships.length; i++) {
            if (!placed.contains(i)) {
                JPanel ship = new JPanel();
                ship.setLayout(null);
                ship.setPreferredSize(new Dimension(Cell.SIZE * ships[i], Cell.SIZE));
                for (int deck = 0; deck < ships[i]; deck++) {
                    JButton cell = new JButton();
                    cell.setFocusPainted(false);
                    cell.setFocusable(false);
                    cell.setRolloverEnabled(false);
                    cell.setSize(new Dimension(Cell.SIZE, Cell.SIZE));
                    cell.setLocation(Cell.SIZE * deck, 0);
                    if (currentShip != null && currentShip == i)
                        cell.setBackground(Color.RED);
                    else
                        cell.setBackground(Color.BLUE);
                    final int finalI = i;
                    cell.addActionListener(e -> {
                        currentShip = finalI;
                        renderShipsBox();
                    });
                    ship.add(cell);
                }
                shipsBox.add(ship);
                shipsBox.add(Box.createVerticalStrut(15));
            }
        }
        shipsBox.revalidate();
    }

    private void clearTargets() {
        targets.forEach(Cell::update);
        targets.clear();
    }
}
