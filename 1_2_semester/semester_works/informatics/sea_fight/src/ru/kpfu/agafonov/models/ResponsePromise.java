package ru.kpfu.agafonov.models;

public interface ResponsePromise {
    void resolve(String message);
}
