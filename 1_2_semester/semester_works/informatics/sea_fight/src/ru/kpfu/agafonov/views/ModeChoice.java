package ru.kpfu.agafonov.views;

import javax.swing.*;
import java.awt.event.ActionListener;

public class ModeChoice extends JPanel {
    public ModeChoice(ActionListener asServer, ActionListener asClient) {
        JButton asServerBtn = new JButton("Начать игру");
        JButton asClientBtn = new JButton("Присоединиться к игре");

        asServerBtn.addActionListener(asServer);
        asClientBtn.addActionListener(asClient);
        this.add(asServerBtn);
        this.add(asClientBtn);
    }
}
