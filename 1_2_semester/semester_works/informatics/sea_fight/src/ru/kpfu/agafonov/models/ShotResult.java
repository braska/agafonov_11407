package ru.kpfu.agafonov.models;

public enum ShotResult {
    MISS(0),
    HIT(1),
    KILL(2),
    NOMORE(3);

    private int value;

    ShotResult(int i) {
        this.value = i;
    }

    public int getValue() {
        return value;
    }

    public static ShotResult fromString(String text) {
        if (text != null) {
            for (ShotResult b : ShotResult.values()) {
                if (text.equalsIgnoreCase(b.value + "")) {
                    return b;
                }
            }
        }
        return null;
    }
}
