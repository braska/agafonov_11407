package ru.kpfu.agafonov.views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class WaitOpponent extends JPanel {
    public WaitOpponent() {
        JLabel imageLabel = new JLabel();
        ImageIcon loader = new ImageIcon(getClass().getResource("/loader.gif"));
        imageLabel.setIcon(loader);
        imageLabel.setBorder(new EmptyBorder(30, 60, 30, 60));
        this.add(imageLabel, BorderLayout.CENTER);
    }
}
