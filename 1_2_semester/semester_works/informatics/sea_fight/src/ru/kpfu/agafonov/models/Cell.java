package ru.kpfu.agafonov.models;

public class Cell {
    Ship ship;
    private Boolean isHit = false;
    private int x, y;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean isHit() {
        return isHit;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void shot() {
        isHit = true;
    }

    public boolean isPartOfShip() {
        return ship != null;
    }

    public Ship getShip() {
        return ship;
    }
}
