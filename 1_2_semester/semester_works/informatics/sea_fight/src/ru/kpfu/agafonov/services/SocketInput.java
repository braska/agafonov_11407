package ru.kpfu.agafonov.services;

import ru.kpfu.agafonov.models.ConnectPromise;
import ru.kpfu.agafonov.models.ResponsePromise;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

public class SocketInput {

    protected DatagramSocket inSocket;
    protected ConnectPromise onOpponentConnect;

    public SocketInput(int port) throws SocketException {
        this(port, null);
    }

    public SocketInput(int port, ConnectPromise onOpponentConnect) throws SocketException {
        this.onOpponentConnect = onOpponentConnect;
        this.inSocket = new DatagramSocket(port);
    }

    /**
     * Wait welcome message from opponent
     */
    public void waitOpponent() {
        new Thread(() -> {
            DatagramPacket packet = new DatagramPacket(new byte[2], 2);
            try {
                inSocket.receive(packet);
                String message = new String(packet.getData());
                if (message.equals("hi")) {
                    onOpponentConnect.resolve(this, packet.getAddress());
                } else {
                    waitOpponent();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    /**
     * Wait some message from opponent
     * @param byteCounter quantity of received bytes
     * @param onResponse callback (will be called after receiving messages)
     */
    public void waitResponse(int byteCounter, ResponsePromise onResponse) {
        new Thread(() -> {
            DatagramPacket packet = new DatagramPacket(new byte[byteCounter], byteCounter);
            try {
                inSocket.receive(packet);
                String message = new String(packet.getData());
                onResponse.resolve(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    /**
     * Close socket
     */
    public void stop(){
        this.inSocket.close();
        System.out.println("Server stoped");
    }
}
