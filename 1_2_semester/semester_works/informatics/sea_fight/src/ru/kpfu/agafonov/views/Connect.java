package ru.kpfu.agafonov.views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

public class Connect extends JPanel {
    JTextField ipField;
    JTextField portField;

    public Connect(ActionListener okClick, ActionListener cancelClick) {
        Box ipBox = Box.createHorizontalBox();
        JLabel ipLabel = new JLabel("IP:");
        ipField = new JTextField(15);
        ipField.setName("ip");
        ipBox.add(ipLabel);
        ipBox.add(Box.createHorizontalStrut(6));
        ipBox.add(ipField);

        Box portBox = Box.createHorizontalBox();
        JLabel portLabel = new JLabel("Порт:");
        portField = new JTextField(5);
        portBox.add(portLabel);
        portBox.add(Box.createHorizontalStrut(6));
        portBox.add(portField);

        Box btnBox = Box.createHorizontalBox();
        JButton ok = new JButton("OK");
        JButton cancel = new JButton("Отмена");
        ok.addActionListener(okClick);
        cancel.addActionListener(cancelClick);
        btnBox.add(Box.createHorizontalGlue());
        btnBox.add(ok);
        btnBox.add(Box.createHorizontalStrut(12));
        btnBox.add(cancel);

        ipLabel.setPreferredSize(portLabel.getPreferredSize());

        Box mainBox = Box.createVerticalBox();
        mainBox.add(ipBox);
        mainBox.add(Box.createVerticalStrut(12));
        mainBox.add(portBox);
        mainBox.add(Box.createVerticalStrut(17));
        mainBox.add(btnBox);
        mainBox.setBorder(new EmptyBorder(12, 12, 12, 12));
        this.add(mainBox);
    }

    public String getIp() {
        return ipField.getText();
    }

    public String getPort() {
        return portField.getText();
    }
}
