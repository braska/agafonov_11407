package ru.kpfu.agafonov.views;

import javax.swing.*;
import java.awt.*;

public class Cell extends JButton {
    private ru.kpfu.agafonov.models.Cell cellModel;
    public final static int SIZE = 30;

    public Cell(ru.kpfu.agafonov.models.Cell cell) {
        this.cellModel = cell;
        this.setSize(new Dimension(SIZE, SIZE));
        update();
    }

    public void update() {
        this.setBackground(Color.WHITE);
        this.setText("");

        if (cellModel.isHit()) {
            if (cellModel.isPartOfShip()) {
                this.setBackground(Color.red);
            } else {
                this.setText("*");
            }
        } else {
            if (cellModel.isPartOfShip()) {
                this.setBackground(Color.BLUE);
            }
        }
    }

    public ru.kpfu.agafonov.models.Cell getCellModel() {
        return cellModel;
    }
}
