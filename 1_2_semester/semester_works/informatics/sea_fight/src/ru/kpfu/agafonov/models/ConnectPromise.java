package ru.kpfu.agafonov.models;

import ru.kpfu.agafonov.services.SocketInput;

import java.net.InetAddress;

public interface ConnectPromise {
    void resolve(SocketInput in, InetAddress address);
}
