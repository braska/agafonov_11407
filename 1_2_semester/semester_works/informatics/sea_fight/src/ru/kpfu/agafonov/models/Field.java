package ru.kpfu.agafonov.models;

import java.util.HashSet;
import java.util.Set;

public class Field extends EventEmmiter {
    private Set<Ship> ships = new HashSet<>();
    private Set<Cell> cells = new HashSet<>();
    private int size = 10;

    /**
     * Init field
     */
    public Field() {
        // Создаём клетки, подписываем каждую клетку на событие убития рядомстоящего корабля.
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                Cell cell = new Cell(i, j);
                for (int ni = Math.max(0, i - 1); ni <= Math.min(size - 1, i + 1); ni++) {
                    for (int nj = Math.max(0, j - 1); nj <= Math.min(size - 1, j + 1); nj++) {
                        this.on("after_near_kill_ship_" + ni + "_" + nj, cell::shot);
                    }
                }
                cells.add(cell);
            }
        }
    }

    /**
     * Get set of all cells
     * @return
     */
    public Set<Cell> getCells() {
        return cells;
    }

    /**
     * Get field size
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     * Get cell model by coords
     * @param x X-axis coord
     * @param y Y-axis coord
     * @return cell model
     */
    public Cell findCell(int x, int y) {
        for (Cell cell : cells) {
            if (cell.getX() == x && cell.getY() == y)
                return cell;
        }
        return null;
    }

    /**
     * Find nearest shit
     * @param cell cell model
     * @return ship model
     */
    public Ship findNearbyShip(Cell cell) {
        for (Ship ship : ships) {
            for (int ni = Math.max(0, cell.getX() - 1); ni <= Math.min(size - 1, cell.getX() + 1); ni++) {
                for (int nj = Math.max(0, cell.getY() - 1); nj <= Math.min(size - 1, cell.getY() + 1); nj++) {
                    if (!(ni == cell.getX() && nj == cell.getY())) {
                        if (ship.isLocatedOn(ni, nj))
                            return ship;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Add ship to field
     * @param ship ship model
     */
    public void addShip(Ship ship) {
        ships.add(ship);
    }

    /**
     * Check alive ships
     * @return true if have alive ships else false
     */
    public boolean noMoreShips() {
        return ships.stream().filter(ship -> !ship.isKilled()).count() == 0;
    }
}
