package ru.kpfu.agafonov.views;

import ru.kpfu.agafonov.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;

public class Menu extends JMenuBar {
    public Menu(ActionListener newGameActionListener, ActionListener exitActionListener) {
        JMenu menu = new JMenu("Игра");
        this.add(menu);
        JMenuItem newGame = new JMenuItem("Новая игра", KeyEvent.VK_N);
        newGame.setAccelerator(KeyStroke.getKeyStroke('N', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        newGame.addActionListener(newGameActionListener);
        menu.add(newGame);
        JMenuItem exit = new JMenuItem("Выход");
        exit.addActionListener(exitActionListener);
        menu.add(exit);
    }
}
