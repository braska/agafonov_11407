package ru.kpfu.agafonov.controllers;

import ru.kpfu.agafonov.models.Cell;
import ru.kpfu.agafonov.models.Field;
import ru.kpfu.agafonov.models.Ship;
import ru.kpfu.agafonov.models.ShotResult;
import ru.kpfu.agafonov.services.GameTransport;
import ru.kpfu.agafonov.services.SocketInput;
import ru.kpfu.agafonov.services.SocketOutput;
import ru.kpfu.agafonov.views.GameOver;
import ru.kpfu.agafonov.views.MainWindow;
import ru.kpfu.agafonov.views.Placement;
import ru.kpfu.agafonov.views.WaitOpponent;

import java.io.IOException;

public class Game {
    private GameTransport gt;
    private Field myField, opponentField;
    private MainWindow window;
    private ru.kpfu.agafonov.views.Game gameView;
    private Boolean isMyShot;
    private Boolean gameOver;
    private Boolean iAmReady = false;
    private Boolean opponentReady = false;

    public Game(MainWindow window, SocketInput in, SocketOutput out, boolean firstShot) {
        this.isMyShot = firstShot;
        this.window = window;
        this.gt = new GameTransport(in, out);
        myField = new Field();

        gt.waitOpponentReady(() -> {
            opponentReady = true;
            if (iAmReady != null && iAmReady)
                start();
        });

        window.setContentPanel(new Placement(myField, () -> {
            try {
                gt.sendIAmReady();
                this.iAmReady = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (opponentReady != null && opponentReady) {
                start();
            } else {
                window.setContentPanel(new WaitOpponent());
                window.setStatusBarText("Оппонент расставляет корабли");
            }
        }, 4, 3, 3, 2, 2, 2, 1, 1, 1, 1));
        window.setStatusBarText("Расставьте корабли");
    }

    private void start() {
        opponentField = new Field();
        gameView = new ru.kpfu.agafonov.views.Game(myField, opponentField, (cellBtn) -> {
            Cell cell = cellBtn.getCellModel();
            if (isMyShot && !cell.isHit()) {
                isMyShot = false;
                try {
                    gt.shot(cell.getX(), cell.getY(), result -> {
                        System.out.println(result);
                        if (result != ShotResult.MISS) {
                            Ship ship = opponentField.findNearbyShip(cell);
                            if (ship == null) {
                                System.out.println("Добавляем корабль");
                                opponentField.addShip(new Ship(cell));
                            } else {
                                ship.addDeck(cell);
                            }
                        }
                        switch (result) {
                            case MISS:
                                cell.shot();
                                break;
                            case HIT:
                                cell.shot();
                                isMyShot = true;
                                break;
                            case KILL:
                                cell.getShip().getCells().forEach(deck -> opponentField.emit("after_near_kill_ship_" + deck.getX() + "_" + deck.getY()));
                                isMyShot = true;
                                break;
                            case NOMORE:
                                cell.getShip().getCells().forEach(deck -> opponentField.emit("after_near_kill_ship_" + deck.getX() + "_" + deck.getY()));
                                gameOver = true;
                                break;
                        }
                        gameView.updateOpponentField();
                        tick();
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        window.setContentPanel(gameView);
        tick();
    }

    private void tick() {
        if (gameOver != null) {
            window.setContentPanel(new GameOver(gameOver));
            window.setStatusBarText("Игра окончена");
        } else {
            if (isMyShot) {
                window.setStatusBarText("Ваш ход");
            } else {
                window.setStatusBarText("Ходит ваш соперник");
                gt.waitShot((x, y) -> {
                    Cell cell = myField.findCell(x, y);
                    cell.shot();
                    ShotResult result = cell.isPartOfShip() ? (cell.getShip().isKilled() ? (myField.noMoreShips() ? ShotResult.NOMORE : ShotResult.KILL) : ShotResult.HIT) : ShotResult.MISS;
                    if (result == ShotResult.KILL || result == ShotResult.NOMORE) {
                        if (result == ShotResult.NOMORE)
                            gameOver = false;
                        cell.getShip().getCells().forEach(deck -> myField.emit("after_near_kill_ship_" + deck.getX() + "_" + deck.getY()));
                    }
                    try {
                        gt.sendShotResult(result);
                        isMyShot = result == ShotResult.MISS;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    gameView.updateMyField();
                    tick();
                });
            }
        }
    }

}
