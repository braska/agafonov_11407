package ru.kpfu.agafonov.models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class EventEmmiter {
    private Map<String, HashSet<Promise>> events = new HashMap<>();

    public void on(String eventName, Promise event) {
        if (events.containsKey(eventName)) {
            events.get(eventName).add(event);
        } else {
            HashSet<Promise> hs = new HashSet<>();
            hs.add(event);
            events.put(eventName, hs);
        }
    }

    public void emit(String eventName) {
        if (events.containsKey(eventName)) {
            events.get(eventName).forEach(Promise::resolve);
        }
    }
}
