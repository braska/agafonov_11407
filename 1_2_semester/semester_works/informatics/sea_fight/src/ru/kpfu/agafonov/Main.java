package ru.kpfu.agafonov;

import ru.kpfu.agafonov.controllers.Game;
import ru.kpfu.agafonov.services.SocketInput;
import ru.kpfu.agafonov.services.SocketOutput;
import ru.kpfu.agafonov.views.*;

import javax.swing.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Properties;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = Main.class.getClass().getResourceAsStream("/config.properties");
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        MainWindow window = new MainWindow(onNewGameEvent -> System.out.println("Ниасилил"));
        window.setContentPanel(new ModeChoice(e -> {
            window.setContentPanel(new WaitOpponent());
            window.setStatusBarText("Ожидание оппонента");
            try {
                SocketInput socketInput = new SocketInput(Integer.parseInt(prop.getProperty("inPort")), (in, address) -> {
                    System.out.println("К нам подконекчиваются");
                    try {
                        System.out.println("Конектимся в ответ");
                        SocketOutput socketOutput = new SocketOutput(new InetSocketAddress(address, Integer.parseInt(prop.getProperty("inPort"))));
                        new Game(window, in, socketOutput, true);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                });
                socketInput.waitOpponent();
            } catch (SocketException e1) {
                e1.printStackTrace();
            }
        }, e -> {
            window.setContentPanel(new Connect(okClickEvent -> {
                Connect connectView = (Connect) ((JButton) okClickEvent.getSource()).getParent().getParent().getParent();
                window.setContentPanel(new WaitOpponent());
                window.setStatusBarText("Ожидание ответного подключения от оппонента");
                try {
                    System.out.println("Создаём исходящее подключение");
                    SocketOutput socketOutput = new SocketOutput(new InetSocketAddress(connectView.getIp(), Integer.parseInt(connectView.getPort())), false);
                    System.out.println("Создаём входящее подключение");
                    SocketInput socketInput = new SocketInput(Integer.parseInt(prop.getProperty("inPort")), (in, address) -> {
                        System.out.println("К нам конектнулись в ответ");
                        new Game(window, in, socketOutput, false);
                    });
                    System.out.println("Запускаем ожидание ответного конекта");
                    socketInput.waitOpponent();
                    System.out.println("Шлём приветственное сообщение");
                    socketOutput.send("hi");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }, cancelClickEvent -> {
                window.setContentPanel((JPanel) ((JButton) e.getSource()).getParent());
                window.setStatusBarText("Выберите режим");
            }));
            window.setStatusBarText("");
        }));
        window.setStatusBarText("Выберите режим");
    }
}
