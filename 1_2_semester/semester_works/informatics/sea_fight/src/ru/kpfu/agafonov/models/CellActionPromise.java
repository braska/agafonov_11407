package ru.kpfu.agafonov.models;

public interface CellActionPromise {
    void resolve(ru.kpfu.agafonov.views.Cell cell);
}
