package ru.kpfu.agafonov.views;

import ru.kpfu.agafonov.models.CellActionPromise;
import ru.kpfu.agafonov.models.Field;

import javax.swing.*;

public class Game extends JPanel {

    private ru.kpfu.agafonov.views.Field myField, opponentField;

    public Game(Field myField, Field opponentField, CellActionPromise onShot) {
        this.myField = new ru.kpfu.agafonov.views.Field(myField);
        this.add(this.myField);
        this.add(Box.createVerticalStrut(30));
        this.opponentField = new ru.kpfu.agafonov.views.Field(opponentField, onShot);
        this.add(this.opponentField);
    }

    public void updateMyField() {
        myField.rerender();
    }

    public void updateOpponentField() {
        opponentField.rerender();
    }
}
