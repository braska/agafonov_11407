package ru.kpfu.agafonov.views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class MainWindow {
    private JFrame frame;
    private JLabel statusBar;
    private Runnable onWindowClose;
    private ActionListener exitActionListener;
    private ActionListener onNewGame;
    private JPanel contentPanel;


    public MainWindow(ActionListener onNewGame) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        this.onNewGame = onNewGame;
        createAndShowGUI();
    }

    public void setStatusBarText(String text) {
        statusBar.setText(text);
    }

    public void setOnWindowClose(Runnable method) {
        onWindowClose = method;
    }

    public void setContentPanel(JPanel contentPanel) {
        if (this.contentPanel != null)
            frame.getContentPane().remove(this.contentPanel);
        this.contentPanel = contentPanel;
        frame.getContentPane().add(contentPanel, BorderLayout.NORTH);
        frame.pack();
        //centreWindow();
    }

    private void createAndShowGUI() {
        frame = new JFrame("Морской бой");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        Menu menu = new Menu(onNewGame, e -> frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING)));

        frame.setJMenuBar(menu);

        statusBar = new JLabel();
        statusBar.setPreferredSize(new Dimension(100, 16));
        frame.getContentPane().add(statusBar, BorderLayout.SOUTH);

        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    if (onWindowClose != null)
                        onWindowClose.run();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });

        frame.pack();
        frame.setLocationByPlatform(true);
        //centreWindow();
        frame.setVisible(true);
    }

    public void centreWindow() {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }
}
