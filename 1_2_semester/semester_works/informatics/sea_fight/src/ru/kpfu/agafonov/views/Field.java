package ru.kpfu.agafonov.views;

import ru.kpfu.agafonov.models.CellActionPromise;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Field extends JPanel {

    private Cell[][] cells;

    public Field(ru.kpfu.agafonov.models.Field fieldModel) {
        this(fieldModel, null);
    }

    public Field(ru.kpfu.agafonov.models.Field fieldModel, CellActionPromise onClick) {
        cells = new Cell[fieldModel.getSize()][fieldModel.getSize()];
        this.setPreferredSize(new Dimension(Cell.SIZE * fieldModel.getSize(), Cell.SIZE * fieldModel.getSize()));
        this.setLayout(null);
        fieldModel.getCells().forEach(cellModel -> {
            Cell cell = new Cell(cellModel);
            cell.setLocation(Cell.SIZE * cellModel.getX(), Cell.SIZE * cellModel.getY());

            if (onClick != null)
                cell.addActionListener(e -> onClick.resolve(cell));
            else {
                cell.setEnabled(false);
            }
            this.add(cell);
            this.cells[cellModel.getX()][cellModel.getY()] = cell;
        });
    }

    public void addPlacementListener(CellActionPromise onClick, CellActionPromise onMouseEnter, CellActionPromise onMouseLeave) {
        for (Cell[] row : this.cells) {
            for (Cell cell : row) {
                cell.setEnabled(true);
                cell.addActionListener(e -> onClick.resolve(cell));
                cell.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        onMouseEnter.resolve(cell);
                    }

                    @Override
                    public void mouseExited(MouseEvent e) {
                        onMouseLeave.resolve(cell);
                    }
                });
            }
        }
    }

    public void rerender() {
        for (Cell[] row : this.cells) {
            for (Cell cell : row) {
                cell.update();
            }
        }
    }

    public Cell getCell(int x, int y) {
        return cells[x][y];
    }
}
