package ru.kpfu.agafonov;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    /**
     * Limit for buffer size
     * Unit: bytes
     */
    public static final int BUFFER_LIMIT = 1024;

    public static void main(String[] args) {
        if(args.length > 0) {

            String filename = args[0];
            int exitCode = 0;

            FileInputStream in = null;

            try {
                in = new FileInputStream(filename);
                Scanner sc = new Scanner(System.in);
                System.out.println("Введите искомую подстроку: ");
                String needle = sc.nextLine();
                int fileSize = in.available();
                int currentCursorPosition = 0;
                ArrayList<Integer> positions = new ArrayList<>();
                while ((fileSize - currentCursorPosition) >= needle.length()) {

                    byte[] bytes = new byte[BUFFER_LIMIT];
                    int forward = in.read(bytes);
                    currentCursorPosition += forward;

                    String haystack = new String(bytes);
                    int position;
                    int offset = 0;
                    while ((position = indexOf(needle, haystack)) != -1) {
                        int fakePosition = position + offset + currentCursorPosition - forward + 1;
                        positions.add(fakePosition);
                        haystack = haystack.substring(position + 1);
                        offset += position + 1;
                    }

                    currentCursorPosition += in.skip(-needle.length() + 1);
                }
                System.out.println(positions);
            } catch (IOException e) {
                e.printStackTrace();
                exitCode = 1;
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                        exitCode = 1;
                    }
                }
                if(exitCode != 0)
                    System.exit(exitCode);
            }

        } else {
            System.out.println("Expected filename as an argument.");
            System.exit(1);
        }
    }

    /**
     *
     * @param needle necessary substring
     * @param haystack source string
     * @return the index of the first occurrence of the specified substring,
     *          or {@code -1} if there is no such occurrence.
     */
    public static int indexOf(String needle, String haystack) {
        if(needle.length() > haystack.length() || needle.length() <= 0)
            return -1;
        HashMap<Character, Integer> stopCharTable = new HashMap<>();

        /* fill the table */
        for (int i = 0; i < needle.length() - 1; i++) {
            stopCharTable.put(needle.charAt(i), needle.length() - i - 1);
        }

        /* start finding */
        for(int haystackPosition = needle.length() - 1; haystackPosition <= haystack.length() - 1; )
        {
            int needlePosition = needle.length() - 1;
            int k = haystackPosition;
            /* we check for needle and haystack starting at the end */
            while(needle.charAt(needlePosition) == haystack.charAt(k))
            {
                if(needlePosition == 0)
                    return k;

                k--;
                needlePosition--;
            }

            /* substring is not found */
            Integer offset = stopCharTable.get(haystack.charAt(haystackPosition));
            haystackPosition += offset != null ? offset : needle.length();
        }

        return -1;
    }
}
