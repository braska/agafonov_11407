package ru.kpfu.agafonov.implementations.runners;

import ru.kpfu.agafonov.abstractions.Human;
import ru.kpfu.agafonov.interfaces.Runnable;

public class Flash extends Human implements Runnable {
    public final static double speed = 100.0;
    public void run(int meters) {
        System.out.println("Флеш побежал");
        try {
            Thread.sleep((int)(meters/speed)*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Флеш прибежал");
    }
}
