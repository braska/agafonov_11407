package ru.kpfu.agafonov.implementations.runners;

import ru.kpfu.agafonov.abstractions.Human;
import ru.kpfu.agafonov.interfaces.Runnable;

public class Bold extends Human implements Runnable {
    public final static double speed = 10.5;
    public void run(int meters) {
        System.out.println("Болт побежал");
        try {
            Thread.sleep((int) (meters / speed) * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Болт прибежал");
    }
}
