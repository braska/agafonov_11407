package ru.kpfu.agafonov.implementations;

import ru.kpfu.agafonov.abstractions.Animal;

public class Wolf extends Animal {
    @Override
    public void move(int distance) {
        System.out.println("Я бегаю на четырёх ногах");
    }
}
