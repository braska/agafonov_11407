package ru.kpfu.agafonov.implementations;

import ru.kpfu.agafonov.abstractions.Animal;
import ru.kpfu.agafonov.enums.State;

public class Dog extends Animal {
    private State state = State.HUNGRY;

    @Override
    public void eat() {
        switch (this.state) {
            case HUNGRY:
                super.eat();
                this.state = State.FULL;
                break;
            case FULL:
                System.out.println("Я сыт!");
        }
    }
}
