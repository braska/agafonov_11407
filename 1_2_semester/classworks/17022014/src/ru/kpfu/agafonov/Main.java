package ru.kpfu.agafonov;

import ru.kpfu.agafonov.abstractions.Animal;
import ru.kpfu.agafonov.implementations.Dog;
import ru.kpfu.agafonov.implementations.runners.Bold;
import ru.kpfu.agafonov.implementations.runners.Carlos;
import ru.kpfu.agafonov.implementations.runners.Flash;
import ru.kpfu.agafonov.interfaces.Runnable;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Dog();
        animal.eat();
        animal.eat();

        Runnable[] runners = {new Bold(), new Carlos(), new Flash()};
        for (final Runnable runner : runners) {
            new Thread(() -> runner.run(100)).start();
        }
    }
}
