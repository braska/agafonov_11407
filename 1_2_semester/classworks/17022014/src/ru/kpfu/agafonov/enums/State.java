package ru.kpfu.agafonov.enums;

public enum State {
    HUNGRY,
    FULL
}
