package ru.kpfu.agafonov.interfaces;

public interface Thinkable {
    public void think();
}
