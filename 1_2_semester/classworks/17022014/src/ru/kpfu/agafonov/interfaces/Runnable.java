package ru.kpfu.agafonov.interfaces;

public interface Runnable {
    public void run(int meters);
}
