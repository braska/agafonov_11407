package ru.kpfu.agafonov.interfaces;

public interface Sleepable {
    public void sleep();
}
