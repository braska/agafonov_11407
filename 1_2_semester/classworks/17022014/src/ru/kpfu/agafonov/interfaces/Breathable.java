package ru.kpfu.agafonov.interfaces;

public interface Breathable {
    public void breathe();
}
