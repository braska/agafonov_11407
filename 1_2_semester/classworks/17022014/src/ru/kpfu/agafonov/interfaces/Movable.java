package ru.kpfu.agafonov.interfaces;

public interface Movable {
    public void move(int distance);
}
