package ru.kpfu.agafonov.abstractions;

import ru.kpfu.agafonov.interfaces.Thinkable;

public abstract class Human extends Animal implements Thinkable {
    @Override
    public void think() {
        System.out.println("Ура! Я могу думать потому, что я человек!");
    }

    @Override
    public void eat() {
        System.out.println("Ном-ном)) Я ем ложкой потому, что я человек!");
    }

    @Override
    public void move(int distance) {
        System.out.println("Я могу ходить. Ногами.");
    }

    @Override
    public void sleep() {
        System.out.println("Я могу спать в кровате потому, что я человек!");
    }
}
