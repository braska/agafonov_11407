package ru.kpfu.agafonov.abstractions;

import ru.kpfu.agafonov.interfaces.Breathable;
import ru.kpfu.agafonov.interfaces.Eatable;
import ru.kpfu.agafonov.interfaces.Movable;
import ru.kpfu.agafonov.interfaces.Sleepable;

public abstract class Animal implements Breathable, Eatable, Movable, Sleepable {
    @Override
    public void breathe() {
        System.out.println("Я дышу!");
    }

    @Override
    public void eat() {
        System.out.println("Я ем как зверь!");
    }

    @Override
    public void sleep() {
        System.out.println("Я спю где попало!");
    }

    @Override
    public void move(int distance) {
        System.out.println("Я двигаюсь!");
    }
}
