package ru.kpfu.agafonov;

import java.io.IOException;
import java.util.*;

public class Translator {
    Map<String, MyTree<String, ArrayList<String>>> dicts = new HashMap<>();
    Storage storage = new FileStorage();

    Translator() {
        dicts.put("main", new MyTree<>());
    }

    public void switchOn(String source) {
        if (dicts.containsKey(source))
            System.out.println("Словарь уже подключен");
        else
            try {
                dicts.put(source, storage.getFrom(source));
                System.out.println("Словарь подключен");
            } catch (IOException e) {
                System.out.println("Словарь не найден");
            }
    }

    public void switchOff(String source) {
        if (dicts.containsKey(source)) {
            dicts.remove(source);
            System.out.println("Словарь отключен");
        } else
            System.out.println("Указанный словарь не был подключен");
    }

    public void addEntry(String original, String... translations) {
        ArrayList<String> transls;
        if(dicts.get("main").containsKey(original))
            transls = new ArrayList<>();
        else
            transls = dicts.get("main").get(original);
        Collections.addAll(transls, translations);
        dicts.get("main").add(original, transls);
    }
}
