package ru.kpfu.agafonov;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;

public class FileStorage implements Storage {
    public MyTree<String, ArrayList<String>> getFrom(String source) throws IOException {
        MyTree<String, ArrayList<String>> result = new MyTree<>();
        Files.readAllLines(FileSystems.getDefault().getPath(source)).forEach((line) -> {
            String[] words = line.split("( )+|(\t)+");
            String key = words[0];
            ArrayList<String> transl = new ArrayList<>();
            transl.addAll(Arrays.asList(words).subList(1, words.length));
            result.add(key, transl);
        });
        return result;
    }
}
