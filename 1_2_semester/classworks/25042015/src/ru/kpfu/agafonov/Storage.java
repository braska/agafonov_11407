package ru.kpfu.agafonov;

import java.io.IOException;
import java.util.ArrayList;

public interface Storage {
    public MyTree<String, ArrayList<String>> getFrom(String source) throws IOException;
}
