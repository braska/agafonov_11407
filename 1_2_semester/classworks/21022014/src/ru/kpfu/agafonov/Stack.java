package ru.kpfu.agafonov;

import java.util.EmptyStackException;

public class Stack<E> {
    private int size = 0;
    private Node<E> last;

    public void push(E e) {
        last = new Node<E>(e, last);
        size++;
    }

    public E pop() {
        if(size > 0) {
            Node<E> last = this.last;
            this.last = last.prev;
            size--;
            return last.item;
        } else
            throw new EmptyStackException();
    }

    public int size() {
        return size;
    }

    private static class Node<E> {
        E item;
        Node<E> prev;

        Node(E element, Node<E> prev) {
            this.item = element;
            this.prev = prev;
        }
    }
}
