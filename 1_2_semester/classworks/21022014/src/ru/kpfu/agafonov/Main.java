package ru.kpfu.agafonov;

public class Main {
    public static void main(String[] args) {
        Stack<String> myStack = new Stack<>();
        myStack.push("Test 1");
        myStack.push("Test 2");
        myStack.push("Test 3");
        System.out.println(myStack.pop());
        System.out.println(myStack.pop());
        System.out.println(myStack.pop());
        System.out.println(myStack.pop());
    }
}
