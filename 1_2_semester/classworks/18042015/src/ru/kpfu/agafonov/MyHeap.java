package ru.kpfu.agafonov;

import java.util.ArrayList;

public class MyHeap<E> {
    private ArrayList<Node<E>> array = new ArrayList<>();
    public E pop() {
        if(array.size() < 1)
            return null;
        E max = array.get(0).elem;
        array.set(0, array.get(array.size() - 1));
        array.remove(array.size() - 1);
        heapify(0);
        return max;
    }

    public void push(E elem, int priority) {
        array.add(new Node<>(elem, priority));
        int i = array.size() - 1;
        while (i >= 1 && array.get((i-1)/2).priority < array.get(i).priority) {
            Node<E> temp = array.get(i);
            array.set(i, array.get((i-1)/2));
            array.set((i-1)/2, temp);
            i = (i-1)/2;
        }
    }

    private void heapify(int root) {
        int left = 2*root+1;
        int right = 2*root+2;
        int largest = root;
        if (left < array.size() && array.get(left).priority > array.get(root).priority) {
            largest = left;
        }
        if (right < array.size() && array.get(right).priority > array.get(largest).priority) {
            largest = right;
        }
        if (largest != root) {
            Node<E> temp = array.get(root);
            array.set(root, array.get(largest));
            array.set(largest, temp);
            heapify(largest);
        }
    }

    private static class Node<E> {
        int priority;
        E elem;

        Node(E elem, int priority) {
            this.elem = elem;
            this.priority = priority;
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < array.size(); i++) {
            stringBuilder.append(array.get(i).priority).append(" ");
        }
        return stringBuilder.toString();
    }
}
