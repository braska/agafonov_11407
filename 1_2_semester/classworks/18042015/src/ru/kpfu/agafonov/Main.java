package ru.kpfu.agafonov;

public class Main {
    public static void main(String[] args) {
        MyHeap<String> test = new MyHeap<>();

        test.push("test", 74);
        test.push("test", 57);
        test.push("test", 48);
        test.push("test", 26);
        test.push("test", 39);
        test.push("test", 58);
        test.push("test", 45);
        test.push("test", 14);
        test.push("test", 35);
        test.push("test", 40);
        test.push("test", 78);
        test.push("test", 89);
        test.push("test", 29);
        test.push("test", 61);
        test.pop();
        System.out.println(test);
    }
}
