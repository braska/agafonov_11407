package ru.kpfu.agafonov;

public class MyTree<K extends Comparable, V> {
    private Node<K, V> root;

    public void add(K key, V value) {
        Node<K, V> node = new Node<K, V>(key, value);
        if (root != null) {
            insertIntoNode(node, root);
        } else {
            root = node;
        }
    }

    private void insertIntoNode(Node<K, V> node, Node<K, V> into) {
        if (node.key.compareTo(into.key) == 0) {
            into.value = node.value;
        } else if (node.key.compareTo(into.key) < 0) {
            if (into.left == null) {
               into.left = node;
            } else {
                insertIntoNode(node, into.left);
            }
        } else {
            if (into.right == null) {
                into.right = node;
            } else {
                insertIntoNode(node, into.right);
            }
        }
    }

    public void remove(K key) {

    }

    public V get(K key) {
        if(root == null)
            return null;
        else
            return getFromSubtree(root, key);
    }

    private V getFromSubtree(Node<K, V> subtreeRoot, K key) {
        if(key.compareTo(subtreeRoot.key) < 0) {
            return getFromSubtree(subtreeRoot.left, key);
        } else if (key.compareTo(subtreeRoot.key) > 0) {
            return getFromSubtree(subtreeRoot.right, key);
        }
        return subtreeRoot.value;
    }

    private static class Node<K extends Comparable, V> {
        Node<K, V> left;
        Node<K, V> right;
        K key;
        V value;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}
