package ru.kpfu.agafonov;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new MyLinkedList<>();
        list.add(1);
        list.add(1, 2);
        list.add(3);
        list.remove(1);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
