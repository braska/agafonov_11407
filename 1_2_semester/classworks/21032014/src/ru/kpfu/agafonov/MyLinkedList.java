package ru.kpfu.agafonov;

public class MyLinkedList<E> implements List<E> {
    private Node<E> first;
    private Node<E> last;
    private int size;

    public void add(E elem) {
        Node<E> n = new Node<>(elem, null);
        if(first == null) {
            first = n;
            last = n;
        } else {
            last.next = n;
            last = n;
        }
        size++;
    }

    public void add(int index, E elem) {
        if(index > size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        Node<E> x = first;
        Node<E> prev = null;
        for (int i = 0; i < index; i++) {
            prev = x;
            x = x.next;
        }
        Node<E> n = new Node<>(elem, x);
        if(prev != null) {
            prev.next = n;
        } else {
            first = n;
        }
        if(x == null) {
            last = n;
        }
    }

    public E remove(int index) {
        if(index > (size - 1) || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        Node<E> x = first;
        Node<E> prev = null;
        for (int i = 0; i < index; i++) {
            prev = x;
            x = x.next;
        }

        if(x.next == null) {
            last = prev;
        }

        if(prev != null) {
            prev.next = x.next;
        } else {
            first = x.next;
        }
        return x.value;
    }

    public int size() {
        return size;
    }

    public E get(int index) {
        Node<E> x = first;
        for (int i = 0; i < index; i++)
            x = x.next;
        return x.value;
    }

    private static class Node<E> {
        E value;
        Node<E> next;

        Node(E value, Node<E> next) {
            this.value = value;
            this.next = next;
        }
    }
}
