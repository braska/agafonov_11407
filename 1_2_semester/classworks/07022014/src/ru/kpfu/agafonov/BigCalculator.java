package ru.kpfu.agafonov;

public class BigCalculator implements SimpleCalculator, TrigCalculator {
    SimpleCalculator simpleCalculator = new SimpleCalculatorImpl();
    TrigCalculator trigCalculator = new TrigCalculatorImpl();

    @Override
    public int sum(int a, int b) {
        return simpleCalculator.sum(a, b);
    }

    @Override
    public double sum(int a, double b) {
        return simpleCalculator.sum(a, b);
    }

    @Override
    public double sum(double a, int b) {
        return simpleCalculator.sum(a, b);
    }

    @Override
    public double sum(double a, double b) {
        return simpleCalculator.sum(a, b);
    }

    @Override
    public int subtraction(int a, int b) {
        return simpleCalculator.subtraction(a, b);
    }

    @Override
    public double subtraction(int a, double b) {
        return simpleCalculator.subtraction(a, b);
    }

    @Override
    public double subtraction(double a, int b) {
        return simpleCalculator.subtraction(a, b);
    }

    @Override
    public double subtraction(double a, double b) {
        return simpleCalculator.subtraction(a, b);
    }

    @Override
    public double division(int a, int b) {
        return simpleCalculator.division(a, b);
    }

    @Override
    public double division(int a, double b) {
        return simpleCalculator.division(a, b);
    }

    @Override
    public double division(double a, int b) {
        return simpleCalculator.division(a, b);
    }

    @Override
    public double division(double a, double b) {
        return simpleCalculator.division(a, b);
    }

    @Override
    public int multiplication(int a, int b) {
        return simpleCalculator.multiplication(a, b);
    }

    @Override
    public double multiplication(int a, double b) {
        return simpleCalculator.multiplication(a, b);
    }

    @Override
    public double multiplication(double a, int b) {
        return simpleCalculator.multiplication(a, b);
    }

    @Override
    public double multiplication(double a, double b) {
        return simpleCalculator.multiplication(a, b);
    }

    @Override
    public double sin(double a) {
        return trigCalculator.sin(a);
    }

    @Override
    public double sin(int a) {
        return trigCalculator.sin(a);
    }

    @Override
    public double cos(double a) {
        return trigCalculator.cos(a);
    }

    @Override
    public double cos(int a) {
        return trigCalculator.cos(a);
    }

    @Override
    public double tg(double a) {
        return trigCalculator.tg(a);
    }

    @Override
    public double tg(int a) {
        return trigCalculator.tg(a);
    }

    @Override
    public double ctg(double a) {
        return trigCalculator.ctg(a);
    }

    @Override
    public double ctg(int a) {
        return trigCalculator.ctg(a);
    }
}
