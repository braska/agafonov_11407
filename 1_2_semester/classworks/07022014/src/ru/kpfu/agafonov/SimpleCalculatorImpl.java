package ru.kpfu.agafonov;

public class SimpleCalculatorImpl implements SimpleCalculator {
    public int sum(int a, int b) {
        return a+b;
    }
    public double sum(int a, double b) {
        return a+b;
    }
    public double sum(double a, int b) {
        return a+b;
    }
    public double sum(double a, double b) {
        return a+b;
    }


    public int subtraction(int a, int b) {
        return a-b;
    }
    public double subtraction(int a, double b) {
        return a-b;
    }
    public double subtraction(double a, int b) {
        return a-b;
    }
    public double subtraction(double a, double b) {
        return a-b;
    }

    public double division(int a, int b) {
        return ((double)a)/b;
    }
    public double division(int a, double b) {
        return a/b;
    }
    public double division(double a, int b) {
        return a/b;
    }
    public double division(double a, double b) {
        return a/b;
    }


    public int multiplication(int a, int b) {
        return a*b;
    }
    public double multiplication(int a, double b) {
        return a*b;
    }
    public double multiplication(double a, int b) {
        return a*b;
    }
    public double multiplication(double a, double b) {
        return a*b;
    }
}
