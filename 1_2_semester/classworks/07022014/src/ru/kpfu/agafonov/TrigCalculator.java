package ru.kpfu.agafonov;

public interface TrigCalculator {
    public double sin(double a);
    public double sin(int a);

    public double cos(double a);
    public double cos(int a);

    public double tg(double a);
    public double tg(int a);

    public double ctg(double a);
    public double ctg(int a);
}
