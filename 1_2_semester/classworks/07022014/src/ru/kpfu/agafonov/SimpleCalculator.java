package ru.kpfu.agafonov;

public interface SimpleCalculator {
    public int sum(int a, int b);
    public double sum(int a, double b);
    public double sum(double a, int b);
    public double sum(double a, double b);


    public int subtraction(int a, int b);
    public double subtraction(int a, double b);
    public double subtraction(double a, int b);
    public double subtraction(double a, double b);

    public double division(int a, int b);
    public double division(int a, double b);
    public double division(double a, int b);
    public double division(double a, double b);


    public int multiplication(int a, int b);
    public double multiplication(int a, double b);
    public double multiplication(double a, int b);
    public double multiplication(double a, double b);
}
