package ru.kpfu.agafonov;

public class TrigCalculatorImpl implements TrigCalculator {
    public double sin(double a) {
        return Math.sin(Math.toRadians(a));
    }
    public double sin(int a) {
        return sin((double) a);
    }

    public double cos(double a) {
        return Math.cos(Math.toRadians(a));
    }
    public double cos(int a) {
        return cos((double) a);
    }

    public double tg(double a) {
        return Math.tan(Math.toRadians(a));
    }
    public double tg(int a) {
        return tg((double) a);
    }

    public double ctg(double a) {
        return 1.0/Math.tan(Math.toRadians(a));
    }
    public double ctg(int a) {
        return ctg((double) a);
    }
}
