package ru.kpfu.agafonov;

import java.io.FileWriter;
import java.io.IOException;

public class Main {
    static Thread thread2 = new Thread(() -> writeToFile("thread2", 2));
    static Thread thread3 = new Thread(() -> writeToFile("thread3", 3));

    public static void writeToFile(String filename, int threadNumber) {
        int i = 0;
        while(i < 300000) {
            if(threadNumber == 2 && i == 600) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            myNotify(i, threadNumber);
            try(FileWriter writer = new FileWriter(filename, false)) {
                String s = threadNumber + "-" + i;
                writer.write(s);
                i++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void myNotify(int i, int threadNumber) {
        if(threadNumber == 1) {
            if(i == 3000)
                thread2.start();
            if(i == 10000)
                thread3.start();
        }
    }

    public static void main(String[] args) {
        Thread thread1 = new Thread(() -> writeToFile("thread1", 1));
        thread1.start();
    }
}
