package ru.kpfu.agafonov.algotest;

import java.util.Arrays;
import java.util.Random;

public class ArraySort extends Thread {

    private int[] arr;
    private String result;

    public ArraySort() {
        arr = new int[10000];
        Random random = new Random();
        for(int i = 0; i < 10000; i++) {
            arr[i] = random.nextInt();
        }
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis();
        Arrays.sort(arr);
        long end = System.currentTimeMillis();
        result = (end - start) + " ms.";
    }

    public String getResult() {
        return result;
    }
}
