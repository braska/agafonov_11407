package ru.kpfu.agafonov.algotest;

public class Main {
    public static void main(String[] args) {
        ArraySort arraySort = new ArraySort();
        arraySort.start();
        try {
            arraySort.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(arraySort.getResult());
    }
}
