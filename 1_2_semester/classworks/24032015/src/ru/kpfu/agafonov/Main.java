package ru.kpfu.agafonov;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try (Client client = new Client("localhost", 15097)){
            for(int i = 800; i < 1000; i++) {
                client.sendAndWaitResponse(i + ".txt");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
