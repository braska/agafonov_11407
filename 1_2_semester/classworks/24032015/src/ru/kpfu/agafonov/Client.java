package ru.kpfu.agafonov;

import java.io.Closeable;
import java.io.IOException;
import java.net.*;

public class Client implements Closeable {
    private DatagramSocket socket;
    private DatagramSocket _receiveSocket;
    private int serverPort;
    private String host;
    public Client(String host, int serverPort) throws SocketException {
        _receiveSocket = new DatagramSocket(serverPort);
        this.socket = new DatagramSocket(serverPort+10);
        this.serverPort = serverPort;
        this.host = host;
    }

    public void sendAndWaitResponse(String req) throws IOException {
        socket.connect(new InetSocketAddress(host, 15099));
        socket.send(new DatagramPacket(req.getBytes(), req.getBytes().length));
        DatagramPacket responsePack = new DatagramPacket(new byte[1024], 1024);
        _receiveSocket.receive(responsePack);
        String response = new String(responsePack.getData()).trim();
        System.out.println(req + ": " + (response.isEmpty() ? "данные отсутствуют" : response));
    }

    public void close() {
        _receiveSocket.close();
        socket.close();
    }
}
