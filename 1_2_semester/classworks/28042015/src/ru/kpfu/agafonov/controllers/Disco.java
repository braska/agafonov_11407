package ru.kpfu.agafonov.controllers;

import ru.kpfu.agafonov.emuns.MusicType;
import ru.kpfu.agafonov.models.Person;
import ru.kpfu.agafonov.models.Song;

import java.util.ArrayList;
import java.util.Random;

public class Disco {
    private Song currentSong;
    private ArrayList<Person> persons = new ArrayList<>();

    public void addPerson(Person p){
        persons.add(p);
    }

    public ArrayList<Person> getPersons(){
        return persons;
    }

    public Song getCurrentSong(){
        return currentSong;
    }

    public void nextSong(){
        Random r = new Random();
        int index = r.nextInt(5);
        nextSong(new Song(MusicType.values()[index]));
    }

    public void nextSong(Song s){
        currentSong = s;
        for(Person p : persons){
            p.listenMusic(currentSong);
        }
    }
}
