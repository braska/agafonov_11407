package ru.kpfu.agafonov.emuns;

public enum MusicType{
    Pop,
    House,
    Rock,
    Rap,
    RnB
}
