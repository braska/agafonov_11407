package ru.kpfu.agafonov.view;

import ru.kpfu.agafonov.controllers.Disco;
import ru.kpfu.agafonov.models.Person;

public class ConsoleView{
    private Disco disco;

    public ConsoleView(Disco d){
        disco = d;
    }
    public void update(){
        System.out.println(disco.getCurrentSong().getType());
        for(Person p : disco.getPersons()){
            System.out.println(p.getName()+ " " + p.getState());
        }
    }
}
