package ru.kpfu.agafonov;

import ru.kpfu.agafonov.controllers.Disco;
import ru.kpfu.agafonov.emuns.MusicType;
import ru.kpfu.agafonov.models.Boy;
import ru.kpfu.agafonov.models.Girl;
import ru.kpfu.agafonov.view.ConsoleView;

public class Main {
    public static void main(String[] args) {
        Disco disco = new Disco();
        disco.addPerson(new Boy("Kim", MusicType.Rock));
        disco.addPerson(new Girl("Irena", MusicType.House));
        disco.addPerson(new Girl("Olga", MusicType.Pop));
        ConsoleView view = new ConsoleView(disco);
        for(int i = 0; i < 10; i++){
            disco.nextSong();
            view.update();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
