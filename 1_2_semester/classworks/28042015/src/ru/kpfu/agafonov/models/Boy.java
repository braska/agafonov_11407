package ru.kpfu.agafonov.models;

import ru.kpfu.agafonov.emuns.MusicType;

public class Boy extends Person {

    public Boy(String name, MusicType... types) {
        super(name, types);
    }

    public void listenMusic(Song song){
        if(preferredMusic.contains(song.getType())){
            dance();
        } else {
            drink();
        }
    }

    protected void drink() {
        state = "I'm drinking";
    }
}
