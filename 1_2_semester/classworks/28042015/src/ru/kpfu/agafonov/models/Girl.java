package ru.kpfu.agafonov.models;

import ru.kpfu.agafonov.emuns.MusicType;

public class Girl extends Person {

    public Girl(String name, MusicType... types) {
        super(name, types);
    }

    public void listenMusic(Song song){
        if(preferredMusic.contains(song.getType())){
            dance();
        } else {
            pickUp();
        }
    }

    protected void pickUp() {
        state = "I'm picking up";
    }
}
