package ru.kpfu.agafonov.models;

import ru.kpfu.agafonov.emuns.MusicType;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class Person{
    protected Set<MusicType> preferredMusic;
    protected String name;
    protected String state = "default";

    public Person(String name, MusicType... types){
        this.name = name;
        preferredMusic = new HashSet<>();
        Collections.addAll(preferredMusic, types);
    }

    public void listenMusic(Song song){
        if(preferredMusic.contains(song.getType())){
            dance();
        }
    }

    protected void dance(){
        state = "I'm dancing";
    }

    public String getName(){
        return name;
    }
    public String getState(){
        return state;
    }
}