package ru.kpfu.agafonov.models;

import ru.kpfu.agafonov.emuns.MusicType;

public class Song{
    private MusicType type;
    private String name;

    public Song(MusicType type){
        this.type = type;
    }

    public MusicType getType(){
        return type;
    }
}
