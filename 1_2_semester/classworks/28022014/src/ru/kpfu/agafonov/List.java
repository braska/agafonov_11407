package ru.kpfu.agafonov;

public interface List<E> {
    public void add(E e);
    public void add(int index, E e);
    public void remove(int index);
    public E get(int index);
    public int size();
    public void addRange(List<E> list);
}
