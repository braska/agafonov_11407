package ru.kpfu.agafonov;

public class Main {
    public static void main(String[] args) {
        List<String> myList = new MyListImpl<String>();
        myList.add("Test 1");
        myList.add("Test 2");
        myList.add(1, "New test");
        myList.add("Test 3");

        for(int i = 0; i < myList.size(); i++) {
            System.out.println(myList.get(i));
        }
    }
}
