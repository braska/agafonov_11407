package ru.kpfu.agafonov;

import java.util.Arrays;

public class MyListImpl<E> implements List<E> {
    private static final int INITIAL_SIZE = 10;
    private Object[] array;
    private int size = 0;

    public MyListImpl() {
        array = new Object[INITIAL_SIZE];
    }

    public void add(E e) {
        if(size > array.length) {
            array = Arrays.copyOf(array, array.length + array.length >> 1);
        }
        array[size] = e;
        size++;
    }

    public void add(int index, E e) {
        if(index >= size)
            throw new IndexOutOfBoundsException();
        if(size > array.length) {
            array = Arrays.copyOf(array, array.length + array.length >> 1);
        }
        System.arraycopy(array, index, array, index + 1, size - index);
        array[index] = e;
        size++;
    }

    public void remove(int index) {
        if(index >= size)
            throw new IndexOutOfBoundsException();
        if(size - index - 1 > 0)
            System.arraycopy(array, index + 1, array, index, size - index - 1);
        size--;
        array[size] = null;
    }

    public E get(int index) {
        if(index >= size)
            throw new IndexOutOfBoundsException();
        return (E) array[index];
    }

    public int size() {
        return size;
    }

    public void addRange(List<E> list) {
        for(int i = 0; i < list.size(); i++) {
            add(list.get(i));
        }
    }
}
