package ru.kpfu.agafonov;

import java.util.ArrayList;

public class GenericsTest {
    public static void main(String[] args) {
        ArrayList x = new ArrayList();
        x.add("Some object");

        ArrayList<String> strings = new ArrayList<>();
        strings.add("Test 1");
    }
}
