package ru.kpfu.agafonov;

public class Main {
    public static void main(String[] args) {
        Integer x1 = 10;
        Integer x2 = 10;
        Integer x3 = 1000;
        Integer x4 = 1000;
        Integer x5 = Integer.parseInt("456");
        int a = x1;
        System.out.println(x1 == x2);
        System.out.println(x3 == x4);
        System.out.println(x3.equals(x4));


        Double y1 = new Double(10.0);
        Double y2 = 10.0;
        Double y3 = Double.parseDouble("123.0");
    }
}
