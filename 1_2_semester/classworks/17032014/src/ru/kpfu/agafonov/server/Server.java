package ru.kpfu.agafonov.server;

import java.io.Closeable;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.HashMap;

public class Server implements Closeable {
    private DatagramSocket socket;
    private HashMap<String, String> data;
    private boolean on = true;
    private int clientPort;
    public Server(int port, int clientPort, HashMap<String, String> array) throws SocketException {
        this.socket = new DatagramSocket(port);
        this.clientPort = clientPort;
        this.data = array;
    }

    public void awaitAndResponse() {
        while(on) {
            try {
                DatagramPacket pack = new DatagramPacket(new byte[1024], 1024);
                socket.receive(pack);
                String request = new String(pack.getData()).trim();
                System.out.println("Request from " + pack.getAddress() + ": " + request);
                socket.connect(pack.getAddress(), clientPort);
                byte[] response;
                try {
                    response = data.get(request).getBytes();
                } catch (NullPointerException e) {
                    response = new byte[0];
                }
                socket.send(new DatagramPacket(response, response.length));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void close() {
        this.on = false;
        socket.close();
    }
}
