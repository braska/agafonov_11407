package ru.kpfu.agafonov.server;

import java.io.*;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        try (Server server = new Server(9898, 5252, readFromFile("data"))) {
            server.awaitAndResponse();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static HashMap<String, String> readFromFile(String filename) throws IOException {
        HashMap<String, String> result = new HashMap<>();
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String line;
        while ((line = reader.readLine()) != null) {
            String[] str = line.split(" ");
            result.put(str[0], str[1]);
        }
        return result;
    }
}
