package ru.kpfu.agafonov.client;

import java.io.Closeable;
import java.io.IOException;
import java.net.*;

public class Client implements Closeable {
    private DatagramSocket socket;
    private int serverPort;
    public Client(int port, int serverPort) throws SocketException {
        this.socket = new DatagramSocket(port);
        this.serverPort = serverPort;
    }

    public void sendAndWaitResponse(String req) throws IOException {
        socket.connect(InetAddress.getByName("localhost"), serverPort);
        socket.send(new DatagramPacket(req.getBytes(), req.getBytes().length));
        DatagramPacket responsePack = new DatagramPacket(new byte[1024], 1024);
        socket.receive(responsePack);
        String response = new String(responsePack.getData()).trim();
        System.out.println(req + ": " + (response.isEmpty() ? "данные отсутствуют" : response));
    }

    public void close() {
        socket.close();
    }
}
