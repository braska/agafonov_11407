package ru.kpfu.agafonov.client;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try (Client client = new Client(5252, 9898)){
            client.sendAndWaitResponse("Агафонов");
            client.sendAndWaitResponse("Иванов");
            client.sendAndWaitResponse("Тестов");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
