package ru.kpfu.agafonov;

public class ThreeArgumentException extends Exception {
    @Override
    public String getMessage() {
        return "Нельзя использовать число 3 в качестве аргумента";
    }
}
