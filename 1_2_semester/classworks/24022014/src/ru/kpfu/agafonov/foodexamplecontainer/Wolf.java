package ru.kpfu.agafonov.foodexamplecontainer;

public class Wolf extends Animal {
    public void eat(Food food) {
        if(food != Food.MEET)
            throw new InedibleFood(this, food);
        System.out.println("Nom-nom. Я \"" + getName() + "\" и я съел \"" + food.toString() + "\"");
    }

    public String getName() {
        return "Wolf";
    }
}
