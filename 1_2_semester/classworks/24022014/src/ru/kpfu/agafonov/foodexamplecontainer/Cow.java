package ru.kpfu.agafonov.foodexamplecontainer;

public class Cow extends Animal {
    public void eat(Food food) {
        if(food != Food.GRASS)
            throw new InedibleFood(this, food);
        System.out.println("Nom-nom. Я \"" + getName() + "\" и я съел \"" + food.toString() + "\"");
    }

    public String getName() {
        return "Cow";
    }
}
