package ru.kpfu.agafonov.foodexamplecontainer;

public enum Food {
    MEET {
        public String toString() {
            return "Мясо";
        }
    },
    GRASS {
        public String toString() {
            return "Мясо";
        }
    };

    public abstract String toString();
}
