package ru.kpfu.agafonov.foodexamplecontainer;

public class InedibleFood extends IllegalArgumentException {
    private String animal, food;

    public InedibleFood(Animal animal, Food food) {
        super();
        this.animal = animal.getName();
        this.food = food.toString();
    }

    @Override
    public String getMessage() {
        return food + " - это несъедобно для \"" + animal + "\"";
    }
}
