package ru.kpfu.agafonov.foodexamplecontainer;

public abstract class Animal {
    public abstract void eat(Food food);
    public abstract String getName();
}
