package ru.kpfu.agafonov;

import ru.kpfu.agafonov.foodexamplecontainer.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        try {
            Scanner sc = null;
            sc.nextInt();
        } catch(NullPointerException e) {
            System.out.println(e.getMessage());
        }

        readFirstByte("test");

        try {
            plus(3, 7);
        } catch (ThreeArgumentException e) {
            e.printStackTrace();
        }

        Animal[] animals = {new Cow(), new Wolf()};

        for (Animal animal : animals) {
            for(Food food : Food.values())
                try {
                    animal.eat(food);
                } catch (InedibleFood e) {
                    System.out.println(e.getMessage());
                }

        }
    }

    public static int readFirstByte(String filename) {
        try(FileInputStream fileInputStream = new FileInputStream(filename)) {
            return fileInputStream.read();
        } catch (IOException | NullPointerException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static int readFirstByte2(String filename) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(filename);
        return fileInputStream.read();
    }

    public static int plus(int a, int b) throws ThreeArgumentException {
        if(a == 3 || b == 3)
            throw new ThreeArgumentException();
        return a + b;
    }
}
