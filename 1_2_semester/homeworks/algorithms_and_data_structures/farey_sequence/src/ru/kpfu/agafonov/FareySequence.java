package ru.kpfu.agafonov;

public interface FareySequence {
    public void generate(int n);
    public FareyValue getFirst();
    public FareyValue getLast();
    public FareyValue get(int index);
    public int size();
    public String toString();
}
