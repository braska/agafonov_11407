package ru.kpfu.agafonov;

import java.util.ArrayList;

public class ArrayFareySequenceImpl extends ArrayList<ArrayFareySequenceImpl.FareyValue> implements FareySequence {

    public void generate(int n) {
        if(n == 1) {
            this.add(new FareyValue(0, 1));
            this.add(new FareyValue(1, 1));
        } else if(n > 0) {
            generate(n - 1);
            int i = 0;
            while (i < this.size() - 1) {
                FareyValue left = this.get(i);
                FareyValue right = this.get(i + 1);
                if((left.divider + right.divider) <= n) {
                    this.add(i + 1, new FareyValue(left.numerator + right.numerator, left.divider + right.divider));
                    i += 2;
                } else {
                    i++;
                }
            }
        }
    }

    public FareyValue getLast() {
        return this.get(this.size() - 1);
    }

    public FareyValue getFirst() {
        return this.get(0);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[");
        for(int i = 0; i < this.size() - 1; i++)
            result.append(this.get(i)).append(", ");
        result.append(this.getLast());
        result.append("]");
        return result.toString();
    }

    public final class FareyValue implements ru.kpfu.agafonov.FareyValue {
        public final int numerator, divider;

        FareyValue(int numerator, int divider) {
            this.numerator = numerator;
            this.divider = divider;
        }

        @Override
        public String toString(){
            return numerator + "/" + divider;
        }
    }
}
