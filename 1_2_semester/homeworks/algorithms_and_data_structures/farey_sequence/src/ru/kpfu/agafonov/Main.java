package ru.kpfu.agafonov;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        int[] ns = {10, 15, 20, 25, 30, 40, 50, 60, 70, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 3000, 5000, 8000, 10000, 10100};

        ArrayList<FareySequence> sequences = new ArrayList<>();
        sequences.add(new FareySequenceOptimalImpl());
        sequences.add(new FareySequenceImpl());
        sequences.add(new ArrayFareySequenceImpl());
        JSONArray algs = new JSONArray();
        for (FareySequence sequence : sequences) {
            System.out.println("Start " + sequence.getClass().getName() + " algorithm.");
            JSONObject alg = new JSONObject();
            alg.put("name", sequence.getClass().getName());
            JSONArray tests = new JSONArray();
            for(int n : ns) {
                System.out.println("Generate Farey sequence with " + n + " pow.");
                JSONArray test = new JSONArray();
                long start = System.currentTimeMillis();
                sequence.generate(n);
                long end = System.currentTimeMillis();
                long time = end - start;
                test.add(n);
                test.add(time);
                tests.add(test);
                if(time > 15000) {
                    break;
                }
            }
            alg.put("data", tests);
            algs.add(alg);
        }
        try(FileWriter file = new FileWriter("tests.js")) {
            file.write("var data = " + algs.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("All generated! Open index.html in browser.");
    }
}
