package ru.kpfu.agafonov;

public class FareySequenceImpl implements FareySequence {
    private FareyValue first, last;
    private int size = 0;

    public void generate(int n) {
        if(n == 1) {
            this.addLast(0, 1);
            this.addLast(1, 1);
        } else if(n > 0) {
            generate(n - 1);
            for (int i = 0; i < size; i++) {
                FareyValue left = this.get(i);
                FareyValue right = left.getNext();
                if (left.getNext() != null && (left.divider + right.divider <= n)) {
                    this.addAfter(left.numerator + right.numerator, left.divider + right.divider, left);
                }
            }
        }
    }

    public FareyValue getFirst() {
        return first;
    }

    public FareyValue getLast() {
        return last;
    }

    public FareyValue get(int index) {
        if (index < (size >> 1)) {
            FareyValue x = first;
            for (int i = 0; i < index; i++)
                x = x.getNext();
            return x;
        } else {
            FareyValue x = last;
            for (int i = size - 1; i > index; i--)
                x = x.getPrev();
            return x;
        }
    }

    public int size() {
        return size;
    }

    private void addLast(int numerator, int divider) {
        FareyValue f = new FareyValue(numerator, divider, last);
        if(first == null) {
            first = f;
        }
        if(last != null)
            last.setNext(f);
        last = f;
        size++;
    }

    private void addAfter(int numerator, int divider, FareyValue left) {
        if(left != null) {
            FareyValue right = left.getNext();
            if(right != null) {
                FareyValue f = new FareyValue(numerator, divider, left);
                left.setNext(f);
                f.setNext(right);
                right.setPrev(f);
                size++;
            } else {
                addLast(numerator, divider);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[");
        for (FareyValue x = first; x.getNext() != null; x = x.getNext())
            result.append(x).append(", ");
        result.append(last);
        result.append("]");
        return result.toString();
    }

    public final class FareyValue implements ru.kpfu.agafonov.FareyValue, Comparable<FareyValue> {
        public final int numerator, divider;
        private FareyValue next, prev;

        FareyValue(int numerator, int divider, FareyValue prev) {
            this.numerator = numerator;
            this.divider = divider;
            this.prev = prev;
        }

        void setNext(FareyValue next) {
            this.next = next;
        }

        void setPrev(FareyValue prev) {
            this.prev = prev;
        }

        FareyValue getNext() {
            return next;
        }

        FareyValue getPrev() {
            return prev;
        }

        @Override
        public String toString(){
            return numerator + "/" + divider;
        }

        @Override
        public int compareTo(FareyValue o){
            return Double.compare((double)numerator / divider, (double)o.numerator / o.divider);
        }
    }
}
