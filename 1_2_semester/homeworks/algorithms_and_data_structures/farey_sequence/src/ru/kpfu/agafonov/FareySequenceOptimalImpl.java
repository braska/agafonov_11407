package ru.kpfu.agafonov;

public class FareySequenceOptimalImpl implements FareySequence {
    private FareyValue first, last;
    private int size = 0;

    public void generate(int n) {
        this.add(0, 1);
        this.add(1, n);
        int k;

        FareyValue f1 = first;
        FareyValue f2 = last;

        while (f2.divider > 1) {
            k = (n + f1.divider) / f2.divider;
            FareyValue temp = f1;
            f1 = f2;
            f2 = this.add(f2.numerator * k - temp.numerator, f2.divider * k - temp.divider);
        }
    }

    public FareyValue getFirst() {
        return first;
    }

    public FareyValue getLast() {
        return last;
    }

    public FareyValue get(int index) {
        if (index < (size >> 1)) {
            FareyValue x = first;
            for (int i = 0; i < index; i++)
                x = x.getNext();
            return x;
        } else {
            FareyValue x = last;
            for (int i = size - 1; i > index; i--)
                x = x.getPrev();
            return x;
        }
    }

    public int size() {
        return size;
    }

    private FareyValue add(int numerator, int divider) {
        FareyValue f = new FareyValue(numerator, divider, last);
        if(first == null)
            first = f;
        if(last != null)
            last.setNext(f);
        last = f;
        size++;
        return f;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[");
        for (FareyValue x = first; x.getNext() != null; x = x.getNext())
            result.append(x).append(", ");
        result.append(last);
        result.append("]");
        return result.toString();
    }

    public final class FareyValue implements ru.kpfu.agafonov.FareyValue {
        public final int numerator, divider;
        private FareyValue next, prev;

        FareyValue(int numerator, int divider, FareyValue prev) {
            this.numerator = numerator;
            this.divider = divider;
            this.prev = prev;
        }

        void setNext(FareyValue next) {
            this.next = next;
        }

        FareyValue getNext() {
            return next;
        }

        FareyValue getPrev() {
            return prev;
        }

        @Override
        public String toString(){
            return numerator + "/" + divider;
        }
    }
}