package ru.kpfu.agafonov;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;

public class NativeJavaArrayList<E> extends ArrayList<E> {
    public static <T> JSONArray bench(int[] ns, T elem) {
        JSONArray tests = new JSONArray();
        ArrayList<T> algorithm = new ArrayList<>();
        JSONObject write = new JSONObject();
        write.put("name", "Write for " + algorithm.getClass().getName());
        JSONArray writeTests = new JSONArray();

        JSONObject read = new JSONObject();
        read.put("name", "Read for " + algorithm.getClass().getName());
        JSONArray readTests = new JSONArray();

        for(int n : ns) {
            System.out.println("Write " + n + " values (for " + algorithm.getClass().getName() + ")");
            JSONArray writeTest = new JSONArray();
            long start = System.nanoTime();
            for(int i = 0; i < n; i++)
                algorithm.add(elem);
            long end = System.nanoTime();
            long time = end - start;
            writeTest.add(n);
            writeTest.add(time);
            writeTests.add(writeTest);

            System.out.println("Read " + n + " values (for " + algorithm.getClass().getName() + ")");
            JSONArray readTest = new JSONArray();
            start = System.nanoTime();
            for(int i = 0; i < n; i++)
                algorithm.get(i);
            end = System.nanoTime();
            time = end - start;
            readTest.add(n);
            readTest.add(time);
            readTests.add(readTest);
            algorithm.clear();
        }
        write.put("data", writeTests);
        read.put("data", readTests);
        tests.add(write);
        tests.add(read);
        return tests;
    }
}
