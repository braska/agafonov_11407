package ru.kpfu.agafonov;

public interface List<E> {
    public boolean add(E e);
    public void add(int index, E e);
    public E remove(int index);
    public E get(int index);
    public int size();
    public void addRange(List<E> list);
    public void clear();
}
