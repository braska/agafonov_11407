package ru.kpfu.agafonov;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class MyArrayQueue<E> extends MyArrayList<E> {

    @Override
    public void add(int index, E e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public E get(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public E remove(int index) {
        throw new UnsupportedOperationException();
    }

    @SuppressWarnings("unchecked")
    public E pull() {
        if(size > 0) {
            E elem = (E) array[0];
            System.arraycopy(array, 1, array, 0, size - 1);
            size--;
            array[size] = null;
            return elem;
        } else {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public E peek() {
        if(size > 0) {
            return (E) array[0];
        } else {
            return null;
        }
    }

    public static <T> JSONArray bench(int[] ns, T elem) {
        JSONArray tests = new JSONArray();
        MyArrayQueue<T> algorithm = new MyArrayQueue<>();
        JSONObject write = new JSONObject();
        write.put("name", "Write for " + algorithm.getClass().getName());
        JSONArray writeTests = new JSONArray();

        JSONObject read = new JSONObject();
        read.put("name", "Read for " + algorithm.getClass().getName());
        JSONArray readTests = new JSONArray();

        for(int n : ns) {
            System.out.println("Write " + n + " values (for " + algorithm.getClass().getName() + ")");
            JSONArray writeTest = new JSONArray();
            long start = System.nanoTime();
            for(int i = 0; i < n; i++)
                algorithm.add(elem);
            long end = System.nanoTime();
            long time = end - start;
            writeTest.add(n);
            writeTest.add(time);
            writeTests.add(writeTest);

            System.out.println("Read " + n + " values (for " + algorithm.getClass().getName() + ")");
            JSONArray readTest = new JSONArray();
            start = System.nanoTime();
            for(int i = 0; i < n; i++)
                algorithm.pull();
            end = System.nanoTime();
            time = end - start;
            readTest.add(n);
            readTest.add(time);
            readTests.add(readTest);
            algorithm.clear();
        }
        write.put("data", writeTests);
        read.put("data", readTests);
        tests.add(write);
        tests.add(read);
        return tests;
    }
}
