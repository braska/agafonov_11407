package ru.kpfu.agafonov;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Arrays;

public class MyArrayList<E> implements List<E> {
    protected static final int INITIAL_SIZE = 10;
    protected Object[] array;
    protected int size = 0;

    public MyArrayList() {
        array = new Object[INITIAL_SIZE];
    }

    public boolean add(E e) {
        if(size >= array.length) {
            array = Arrays.copyOf(array, array.length + (array.length >> 1));
        }
        array[size] = e;
        size++;
        return true;
    }

    public void add(int index, E e) {
        if(index < 0 || index >= size)
            throw new IndexOutOfBoundsException();
        if(size > array.length) {
            array = Arrays.copyOf(array, array.length + array.length >> 1);
        }
        System.arraycopy(array, index, array, index + 1, size - index);
        array[index] = e;
        size++;
    }

    @SuppressWarnings("unchecked")
    public E remove(int index) {
        if(index < 0 || index >= size)
            throw new IndexOutOfBoundsException();
        E elem = (E) array[index];
        if(size - index - 1 > 0)
            System.arraycopy(array, index + 1, array, index, size - index - 1);
        size--;
        array[size] = null;
        return elem;
    }

    @SuppressWarnings("unchecked")
    public E get(int index) {
        if(index < 0 || index >= size)
            throw new IndexOutOfBoundsException();
        return (E) array[index];
    }

    public int size() {
        return size;
    }

    public void addRange(List<E> list) {
        for(int i = 0; i < list.size(); i++) {
            add(list.get(i));
        }
    }

    public void clear() {
        if(size > 0) {
            array = new Object[INITIAL_SIZE];
            size = 0;
        }
    }

    public static <T> JSONArray bench(int[] ns, T elem) {
        JSONArray tests = new JSONArray();
        MyArrayList<T> algorithm = new MyArrayList<>();
        JSONObject write = new JSONObject();
        write.put("name", "Write for " + algorithm.getClass().getName());
        JSONArray writeTests = new JSONArray();

        JSONObject read = new JSONObject();
        read.put("name", "Read for " + algorithm.getClass().getName());
        JSONArray readTests = new JSONArray();

        for(int n : ns) {
            System.out.println("Write " + n + " values (for " + algorithm.getClass().getName() + ")");
            JSONArray writeTest = new JSONArray();
            long start = System.nanoTime();
            for(int i = 0; i < n; i++)
                algorithm.add(elem);
            long end = System.nanoTime();
            long time = end - start;
            writeTest.add(n);
            writeTest.add(time);
            writeTests.add(writeTest);

            System.out.println("Read " + n + " values (for " + algorithm.getClass().getName() + ")");
            JSONArray readTest = new JSONArray();
            start = System.nanoTime();
            for(int i = 0; i < n; i++)
                algorithm.get(i);
            end = System.nanoTime();
            time = end - start;
            readTest.add(n);
            readTest.add(time);
            readTests.add(readTest);
            algorithm.clear();
        }
        write.put("data", writeTests);
        read.put("data", readTests);
        tests.add(write);
        tests.add(read);
        return tests;
    }
}
