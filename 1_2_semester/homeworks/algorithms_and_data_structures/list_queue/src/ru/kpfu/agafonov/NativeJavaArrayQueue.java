package ru.kpfu.agafonov;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Collection;
import java.util.concurrent.ArrayBlockingQueue;

public class NativeJavaArrayQueue<E> extends ArrayBlockingQueue<E> {

    public NativeJavaArrayQueue(int capacity) {
        super(capacity);
    }

    public NativeJavaArrayQueue(int capacity, boolean fair) {
        super(capacity, fair);
    }

    public NativeJavaArrayQueue(int capacity, boolean fair, Collection<? extends E> c) {
        super(capacity, fair, c);
    }

    public static <T> JSONArray bench(int[] ns, T elem) {
        JSONArray tests = new JSONArray();
        JSONObject write = new JSONObject();
        write.put("name", "Write for java.util.concurrent.ArrayBlockingQueue");
        JSONArray writeTests = new JSONArray();

        JSONObject read = new JSONObject();
        read.put("name", "Read for java.util.concurrent.ArrayBlockingQueue");
        JSONArray readTests = new JSONArray();

        for(int n : ns) {
            ArrayBlockingQueue<T> algorithm = new ArrayBlockingQueue<>(n);
            System.out.println("Write " + n + " values (for " + algorithm.getClass().getName() + ")");
            JSONArray writeTest = new JSONArray();
            long start = System.nanoTime();
            for(int i = 0; i < n; i++)
                algorithm.add(elem);
            long end = System.nanoTime();
            long time = end - start;
            writeTest.add(n);
            writeTest.add(time);
            writeTests.add(writeTest);

            System.out.println("Read " + n + " values (for " + algorithm.getClass().getName() + ")");
            JSONArray readTest = new JSONArray();
            start = System.nanoTime();
            for(int i = 0; i < n; i++)
                algorithm.poll();
            end = System.nanoTime();
            time = end - start;
            readTest.add(n);
            readTest.add(time);
            readTests.add(readTest);
        }
        write.put("data", writeTests);
        read.put("data", readTests);
        tests.add(write);
        tests.add(read);
        return tests;
    }
}
