package ru.kpfu.agafonov;

import org.json.simple.JSONArray;

import java.io.FileWriter;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        int[] ns = {100, 1000, 10000, 100000, 1000000};

        JSONArray tests = new JSONArray();

        Thread myArrayListBenchThread = new Thread(() -> tests.addAll(MyArrayList.bench(ns, "Test value")));
        Thread myArrayQueueBenchThread = new Thread(() -> tests.addAll(MyArrayQueue.bench(ns, "Test value")));
        Thread nativeJavaArrayListBenchThread = new Thread(() -> tests.addAll(NativeJavaArrayList.bench(ns, "Test value")));
        Thread nativeJavaArrayQueueBenchThread = new Thread(() -> tests.addAll(NativeJavaArrayQueue.bench(ns, "Test value")));
        myArrayListBenchThread.start();
        myArrayQueueBenchThread.start();
        nativeJavaArrayListBenchThread.start();
        nativeJavaArrayQueueBenchThread.start();

        try {
            myArrayListBenchThread.join();
            myArrayQueueBenchThread.join();
            nativeJavaArrayListBenchThread.join();
            nativeJavaArrayQueueBenchThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try(FileWriter file = new FileWriter("tests.js")) {
            file.write("var data = " + tests.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
