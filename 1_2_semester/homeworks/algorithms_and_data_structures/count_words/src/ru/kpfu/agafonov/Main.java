package ru.kpfu.agafonov;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        Analyzer analyzer = new Analyzer(args);
        try {
            long start = System.currentTimeMillis();
            analyzer.start();
            long end = System.currentTimeMillis();
            System.out.println("Подсчитано за " + (end - start) + " мс.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
