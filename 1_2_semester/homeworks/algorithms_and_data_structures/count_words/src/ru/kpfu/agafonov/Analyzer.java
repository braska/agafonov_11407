package ru.kpfu.agafonov;

import org.apache.commons.cli.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Analyzer {
    private Integer minLength;
    private boolean cs = false;
    private Options options;
    private String filename;
    private String outputFilename;
    private HashMap<String, Integer> words = new HashMap<>();
    private ValueComparator bvc =  new ValueComparator(words);
    private TreeMap<String, Integer> sortedWords = new TreeMap<>(bvc);
    private int wordCount = 0;

    public Analyzer(String[] inputArgs) {
        this.options = new Options();
        options.addOption("m", true, "set minimal word length");
        options.addOption("c", false, "enable case sensitivity");
        options.addOption("h", "help", false, "help message");

        CommandLineParser parser = new BasicParser();

        try {
            CommandLine cmd = parser.parse(options, inputArgs);
            if(cmd.hasOption('h'))
                printHelp();
            cs = cmd.hasOption("cs");
            minLength = cmd.hasOption("m") ? Integer.parseInt(cmd.getOptionValue("m")) : null;
            String[] args = cmd.getArgs();
            if(args.length < 1)
                throw new MissingArgumentException("Need to specify the file");
            else if(args.length > 1)
                outputFilename = args[1];
            filename = args[0];
        } catch (ParseException|NumberFormatException e) {
            System.out.println(e.getMessage());
            printHelp();
        }

    }

    public void start() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(filename));
        String line;
        while((line = br.readLine()) != null) {
            analyzeLine(line);
        }
        sortedWords.putAll(words);
        if(outputFilename != null) {
            PrintWriter out = new PrintWriter(outputFilename);
            sortedWords.forEach((key, value) -> out.println(key + ": " + value));
            out.println("Total words: " + wordCount);
            out.println("Unique words: " + this.words.size());
            out.close();
        } else {
            sortedWords.forEach((key, value) -> System.out.println(key + ": " + value));
            System.out.println("Total words: " + wordCount);
            System.out.println("Unique words: " + this.words.size());
        }
    }

    private void analyzeLine(String line) {
        String[] words = line.split(" ");
        for(String word : words) {
            String filteredWord = filter(word);
            if(filteredWord != null) {
                if(!cs)
                    filteredWord = filteredWord.toLowerCase();
                if(this.words.containsKey(filteredWord))
                    this.words.replace(filteredWord, this.words.get(filteredWord) + 1);
                else
                    this.words.put(filteredWord, 1);
                wordCount++;
            }
        }
    }

    private String filter(String word) {
        Pattern p = Pattern.compile("^[\\p{Punct} «»–]*+([^ ]." + ((minLength == null || minLength < 2) ? "*?" : ("{" + (minLength - 2) + ",}?[^\\p{Punct} «»–]")) + ")[\\p{Punct} «»–]*+$");
        Matcher m = p.matcher(word);
        return m.matches() ? m.group(1) : null;
    }

    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("... [OPTIONS] FILE [OUTPUT_FILE]", options);
        System.exit(2);
    }
}
